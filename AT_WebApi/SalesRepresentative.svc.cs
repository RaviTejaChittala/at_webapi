﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace AT.Sybiz.WebApi
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SalesRepresentative" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SalesRepresentative.svc or SalesRepresentative.svc.cs at the Solution Explorer and start debugging.
    public class SalesRepresentative : ISalesRepresentative
    {
        public string GetSalesRepresentative()
        {
            return string.Format("You entered: {0}", 3);
        } 
    }
}
