﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AT.Sybiz.Library;
using AT.Sybiz.Library.AT.Model;
using Sybiz.Vision.Platform.Inventory;
using Sybiz.Vision.Platform.Debtors;
using Sybiz.Vision.Platform.Debtors.Transaction;
using Sybiz.Vision.Platform.Creditors;
using AT_BusinessLayer.AT.Model;
using AT_BusinessLayer;

namespace AT.Sybiz.WebApi
{
    [ServiceContract]
    public interface IATSybiz
    {
        #region "Products"

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetAllProduct")]
        List<ProductModel> GetAllProduct();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetAllProductGroup")]
        ProductGroupInfoList GetAllProductGroup();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetProductInfoBySKU?sku={sku}")]
        ProductModel GetProductInfoBySKU(string sku);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetModifiedProductsFromDate?lastSyncDate={lastSyncDate}")]
        List<ProductModel> GetModifiedProductsFromDate(string lastSyncDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetCreatedProductsFromDate?lastSyncDate={lastSyncDate}")]
        List<ProductModel> GetCreatedProductsFromDate(string lastSyncDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDeletedProductsFromDate?lastSyncDate={lastSyncDate}")]
        List<ProductModel> GetDeletedProductsFromDate(string lastSyncDate);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.WrappedRequest,
                                   UriTemplate = "GetAllProductWithinLimit?start={start}&limit={limit}")]
        List<ProductModel> GetAllProductWithinLimit(int start, int limit);

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetInventoryDetailAnalytic")]
        List<ProductQuantitiesToUpdate> GetInventoryDetailAnalytic();
        #endregion

        #region "Sales Representative"

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetAllSalesRepresentative")]
        //SalesRepresentativeInfoList GetAllSalesRepresentative();
        List<SalesRepresentativeModel> GetAllSalesRepresentative();


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetCreatedSalesRepresentativesFromDate?lastSyncDate={lastSyncDate}")]
        List<SalesRepresentativeModel> GetCreatedSalesRepresentativesFromDate(string lastSyncDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDeletedSalesRepresentativesFromDate?lastSyncDate={lastSyncDate}")]
        List<SalesRepresentativeModel> GetDeletedSalesRepresentativesFromDate(string lastSyncDate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.WrappedResponse,
                                    UriTemplate = "CreateSalesRepresentative")]
        ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj);

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
        //                            RequestFormat = WebMessageFormat.Json,
        //                            BodyStyle = WebMessageBodyStyle.Bare,
        //                            UriTemplate = "CreateSalesRepresentative")]
        //ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj);


        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
        //                            RequestFormat = WebMessageFormat.Json,
        //                            BodyStyle = WebMessageBodyStyle.Bare,
        //                            UriTemplate = "UpdateSalesRepresentative?Id={Id}")]
        //ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.WrappedResponse,
                                    UriTemplate = "UpdateSalesRepresentative?Id={id}")]
        ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id);

        #endregion

        #region "Customer"

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetCreatedCustomersFromDate?lastSyncDate={lastSyncDate}")]
        List<CustomerModel> GetCreatedCustomersFromDate(string lastSyncDate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "SetDeliveryAddressAsPrimary?custId={custId}&addressId={addressId}")]
        ResponseResult SetDeliveryAddressAsPrimary(int custId, int addressId);



        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDeletedCustomersFromDate?lastSyncDate={lastSyncDate}")]
        List<CustomerModel> GetDeletedCustomersFromDate(string lastSyncDate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "CreateCustomer")]
        ResponseResult CreateCustomer(CustomerModel CObj);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "UpdateCustomer?Id={Id}")]
        ResponseResult UpdateCustomer(CustomerModel Cobj, int id);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "InsertCustomerAddress")]
        ResponseResult InsertCustomerAddress(DeliveryAddress1 DAObj);

        #endregion

        #region "Supplier"


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetAllSuppliers")]
        SupplierInfoList GetAllSuppliers();

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetCreatedSuppliersFromDate?lastSyncDate={lastSyncDate}")]
        List<SupplierModel> GetCreatedSuppliersFromDate(string lastSyncDate);


        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Bare,
                                   UriTemplate = "GetDeletedSuppliersFromDate?lastSyncDate={lastSyncDate}")]
        List<SupplierModel> GetDeletedSuppliersFromDate(string lastSyncDate);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "CreateSupplier")]
        ResponseResult CreateSupplier(SupplierModel SObj);

        #endregion

        //[OperationContract]
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
        //                            RequestFormat = WebMessageFormat.Json,
        //                            BodyStyle = WebMessageBodyStyle.WrappedResponse,
        //                            UriTemplate = "CreateSalesOrder")]
        //void CreateSalesOrder(SalesOrderModel SOObj);

        [OperationContract]
        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json,
                                    RequestFormat = WebMessageFormat.Json,
                                    BodyStyle = WebMessageBodyStyle.Bare,
                                    UriTemplate = "CreateSalesOrder")]
        ResponseResult CreateSalesOrder(SalesOrderModel SOObj);


        //[OperationContract]
        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
        //                           BodyStyle = WebMessageBodyStyle.Bare,
        //                           UriTemplate = "GetSalesOrder")]
        //SalesOrder GetSalesOrder();
    }
}
