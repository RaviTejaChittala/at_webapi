﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace AT.Sybiz.WebApi
{    
    [ServiceContract]
    public interface ISalesOrder
    {     
        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Wrapped,
                                   UriTemplate = "SalesOrder")]
        string GetSalesOrder();
    }
}
