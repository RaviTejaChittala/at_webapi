﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AT.Sybiz.Library;
using AT.Sybiz.Library.AT.DAO;
using AT.Sybiz.Library.AT.Model;
using Sybiz.Vision.Platform.Inventory;
using Sybiz.Vision.Platform.Security;
using System.Web.Configuration;
using Sybiz.Vision.Platform.Debtors;
using System.Web.Script.Serialization;
using Sybiz.Vision.Platform.Creditors;
using Sybiz.Vision.Platform.Debtors.Transaction;
using AT_BusinessLayer.AT.Model;
using AT_BusinessLayer;
using Csla.Server;
using Newtonsoft.Json;




namespace AT.Sybiz.WebApi
{

    public class ATSybiz : IATSybiz
    {
        static IDataAccess oDAO = new ATDAO();
        //string connectionString = WebConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString;

        #region "Products"

        /// <summary>
        /// Method to get all products
        /// </summary>
        /// <returns></returns>
        public List<ProductModel> GetAllProduct()
        {
            var products = oDAO.GetAllProducts();

            return products;

            //return ProductInfoList.GetList();   
            //var jsonSerialiser = new JavaScriptSerializer();
            //var json = jsonSerialiser.Serialize(oDAO.GetAllProducts());
            //return json;
        }


        /// <summary>
        /// Method to get all products within a limit
        /// </summary>
        /// <returns></returns>
        public List<ProductModel> GetAllProductWithinLimit(int start, int limit)
        {
            var products = oDAO.GetAllProductsWithinLimit(start, limit);

            return products;
            //return ProductInfoList.GetList();   
            //var jsonSerialiser = new JavaScriptSerializer();
            //var json = jsonSerialiser.Serialize(oDAO.GetAllProducts());
            //return json;
        }



        /// <summary>
        /// Get product info by SKU
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public ProductModel GetProductInfoBySKU(string sku)
        {
            return oDAO.GetProductInfoBySKU(sku);
        }

        /// <summary>
        /// Get all product groups
        /// </summary>
        /// <returns></returns>
        public ProductGroupInfoList GetAllProductGroup()
        {
            return oDAO.GetProductGroup();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lastSyncDate"></param>
        /// <returns></returns>
        public List<ProductModel> GetModifiedProductsFromDate(string lastSyncDate)
        {
            return oDAO.GetModifiedProductsFromDate(lastSyncDate);
        }

        public List<ProductModel> GetCreatedProductsFromDate(string lastSyncDate)
        {
            return oDAO.GetCreatedProductsFromDate(lastSyncDate);
        }

        public List<ProductModel> GetDeletedProductsFromDate(string lastSyncDate)
        {
            return oDAO.GetDeletedProductsFromDate(lastSyncDate);
        }

        public List<ProductQuantitiesToUpdate> GetInventoryDetailAnalytic()
        {
            return oDAO.GetInventoryDetailAnalytic();
        }
        #endregion

        #region "Sales Representative"

        /// <summary>
        /// Get all sales representatives
        /// </summary>
        /// <returns></returns>
        //public SalesRepresentativeInfoList GetAllSalesRepresentative()
        //{
        //    Utility.IsLoginToSybiz();
        //    return oDAO.GetSalesRepresentative();
        //}

        public List<SalesRepresentativeModel> GetAllSalesRepresentative()
        {
            return oDAO.GetSalesRepresentative();

        }

        public List<SalesRepresentativeModel> GetCreatedSalesRepresentativesFromDate(string lastSyncDate)
        {
            return oDAO.GetCreatedSalesRepresentativesFromDate(lastSyncDate);

        }

        public List<SalesRepresentativeModel> GetDeletedSalesRepresentativesFromDate(string lastSyncDate)
        {
            return oDAO.GetDeletedSalesRepresentativesFromDate(lastSyncDate);
        }

        public ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj)
        {
            return oDAO.CreateSalesRepresentative(SRObj);
        }

        //public ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj)
        //{
        //    return oDAO.CreateSalesRepresentative(SRObj);
        //}

        //public ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id)
        //{
        //    return oDAO.UpdateSalesRepresentative(SRObj, id);
        //}

        public ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id)
        {
            return oDAO.UpdateSalesRepresentative(SRObj, id);
        }

        #endregion

        #region "Customer"

        public List<CustomerModel> GetCreatedCustomersFromDate(string lastSyncDate)
        {

            return oDAO.GetCreatedCustomersFromDate(lastSyncDate);
        }

        // List<CustomerDetailInfo> GetCreatedCustomersFromDate(string lastSyncDate);

        public ResponseResult SetDeliveryAddressAsPrimary(int custId, int addressId)
        {
            return oDAO.SetDeliveryAddressAsPrimary(custId, addressId);
        }

        public List<CustomerModel> GetDeletedCustomersFromDate(string lastSyncDate)
        {
            return oDAO.GetDeletedCustomersFromDate(lastSyncDate);
        }

        public ResponseResult CreateCustomer(CustomerModel CObj)
        {
            return oDAO.CreateCustomer(CObj);
        }

        public ResponseResult UpdateCustomer(CustomerModel CObj, int id)
        {
            return oDAO.UpdateCustomer(CObj, id);
        }

        public ResponseResult InsertCustomerAddress(DeliveryAddress1 DAObj)
        {
            return oDAO.InsertCustomerAddress(DAObj);
        }

        #endregion

        #region "Supplier"

        public SupplierInfoList GetAllSuppliers()
        {

            return oDAO.GetSupplier();
        }

        public List<SupplierModel> GetCreatedSuppliersFromDate(string lastSyncDate)
        {
            return oDAO.GetCreatedSuppliersFromDate(lastSyncDate);
        }

        public List<SupplierModel> GetDeletedSuppliersFromDate(string lastSyncDate)
        {
            return oDAO.GetDeletedSuppliersFromDate(lastSyncDate);
        }

        public ResponseResult CreateSupplier(SupplierModel SObj)
        {
            return oDAO.CreateSupplier(SObj);
        }




        #endregion

        //public void CreateSalesOrder(SalesOrderModel SOObj)
        //{
        //    oDAO.CreateSalesOrder(SOObj);
        //}

        public ResponseResult CreateSalesOrder(SalesOrderModel SOObj)
        {
            //SalesOrderModel OSOBJ = JsonConvert.DeserializeObject<SalesOrderModel>(SOObj);
            return oDAO.CreateSalesOrder(SOObj);
        }

        //public SalesOrder GetSalesOrder()
        //{
        //    return oDAO.GetSalesOrder();
        //}

    }
}
//2015-08-26 23:33:30.163