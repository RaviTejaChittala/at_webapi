﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using AT_BusinessLayer;

namespace AT_WebApi
{
    [ServiceContract]
    public interface IProduct
    {

        [OperationContract]
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json,
                                   BodyStyle = WebMessageBodyStyle.Wrapped,
                                   UriTemplate = "GetProduct")]
        string GetProduct();
    }    
}
