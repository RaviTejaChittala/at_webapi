﻿using AT.Sybiz.Library;
using AT_BusinessLayer.AT.DAO;
using Sybiz.Vision.Platform.Core.Enumerations;
using Sybiz.Vision.Platform.Creditors;
using Sybiz.Vision.Platform.Debtors;
using Sybiz.Vision.Platform.Debtors.Transaction;
using Sybiz.Vision.Platform.Security;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;

namespace AT_BusinessLayer
{
    public static class Utility
    {

        //Private Const CONNECTION_STRING As String = "Persist Security Info=False;Integrated Security=SSPI;database=VisionUTC2;server=DEV-MARJON\SQL2014;MultipleActiveResultSets=True"
        //Private Const COMMON_CONNECTION_STRING As String = "Persist Security Info=False;Integrated Security=SSPI;database=VisionCommon;server=DEV-MARJON\SQL2014;MultipleActiveResultSets=True"


        public static List<T> DataReaderMapToList<T>(IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();

                obj.GetType().GetProperties().ToList().ForEach(prop =>
                {
                    if (!object.Equals(dr[prop.Name], DBNull.Value))
                    {
                        prop.SetValue(obj, dr[prop.Name], null);
                    }
                });


                //foreach (PropertyInfo prop in obj.GetType().GetProperties())
                //{
                //    if (!object.Equals(dr[prop.Name], DBNull.Value))
                //    {
                //        prop.SetValue(obj, dr[prop.Name], null);
                //    }
                //}
                list.Add(obj);
            }
            return list;
        }

        public static List<T> DataReaderMapToList<T>(DataTable dt)
        {
            List<T> list = new List<T>();
            Type targetType = typeof(T);

            dt.Rows.Cast<DataRow>().ToList().ForEach(dr =>
                                    {
                                        T obj = Activator.CreateInstance<T>();// (System.Runtime.Serialization.FormatterServices.GetUninitializedObject(targetType));// Activator.CreateInstance(//default(T);

                                        obj.GetType().GetProperties().ToList().ForEach(prop =>
                                        {

                                            if (dr.Table.Columns.Contains(prop.Name))
                                            {
                                                if (!object.Equals(dr[prop.Name], DBNull.Value))
                                                {
                                                    //if (object.Equals(dr[prop.Name], "AddressTypeId"))
                                                    //{

                                                    //}
                                                    if (prop.Name == "Postal" || prop.Name == "Physical" || prop.Name == "Delivery")
                                                        prop.SetValue(obj, String.Concat('{', dr[prop.Name], '}'), null);
                                                    else
                                                        prop.SetValue(obj, dr[prop.Name], null);
                                                }
                                            }

                                        });
                                        list.Add(obj);
                                    });
            //foreach (DataRow dr in dt.Rows)
            //{
            //    //obj = Activator.CreateInstance(targetType){};//System.Runtime.Serialization.FormatterServices.GetUninitializedObject((Type)T); //

            //}

            return list;
        }

        public static string GetConnectionString()
        {
            //return "MultipleActiveResultSets=True;Application Name=Sybiz Vision;Connect Timeout=60;Persist Security Info=False;User ID=sa;Password=hrhk;database=annabelltrends;server=DAFFODIL-107\\SQLEXPRESS2012;";

            return ConfigurationManager.AppSettings["DevConnectionString"];
            //
            //MultipleActiveResultSets=True;Application Name=Sybiz Vision;Connect Timeout=60;Persist Security Info=False;User ID=sa;Password=hrhk;database=annabelltrends;server=DAFFODIL-107\SQLEXPRESS2012;
            //    ConfigurationManager.ConnectionStrings["DevConnectionString"].ConnectionString;
        }

        public static string GetCommonConnectionString()
        {
            //return "Persist Security Info=False;User ID=sa;Password=hrhk;database=Vision;server=DAFFODIL-107\\SQLEXPRESS2012;;MultipleActiveResultSets=True;";

            return ConfigurationManager.AppSettings["DevCommonConnectionString"];

            // 
            //"Persist Security Info=False;User ID=sa;Password=hrhk;database=Vision;server=DAFFODIL-107\\SQLEXPRESS2012;;MultipleActiveResultSets=True";

            //ConfigurationManager.ConnectionStrings["DevCommonConnectionString"].ConnectionString;
        }

        public static bool IsLoginToSybiz()
        {
            bool IsLogin = false;
            try
            {
                if (Principal.CurrentPrincipal() != null)
                    Principal.LogOut();

                IsLogin = Principal.LogIn(ConfigurationManager.AppSettings["SybizUserName"], ConfigurationManager.AppSettings["SybizPassword"], GetConnectionString(), GetCommonConnectionString());
                return IsLogin;

            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
                return IsLogin;
            }

            ////Sybiz.Vision.Platform.Debtors.Transaction.ICustomerJournalTransaction.Transaction.ProcessingEngine.Execute(header)
            //return true;
            ////Principal.LogIn(ConfigurationManager.AppSettings["SybizUserName"], ConfigurationManager.AppSettings["SybizPassword"], GetConnectionString(), COMMON_CONNECTION_STRING);
        }

        public static SupplierTaxStatus GetSupplierTaxStatus(int id)
        {
            switch (id)
            {
                case 0: return SupplierTaxStatus.Import;
                case 1: return SupplierTaxStatus.NonTaxable;
                case 2: return SupplierTaxStatus.Taxable;
                default: return SupplierTaxStatus.Taxable;
            }

        }

        public static WithholdingAmountWithheldMethod GetWithholdingAmountWithheld(int withholdingMethodId)
        {
            switch (withholdingMethodId)
            {
                case 0: return WithholdingAmountWithheldMethod.Always;
                case 1: return WithholdingAmountWithheldMethod.None;
                case 2: return WithholdingAmountWithheldMethod.NoTaxFileNumber;
                default: return WithholdingAmountWithheldMethod.None;
            }
        }

        public static CustomerTaxStatus GetCustomerTaxStatus(int id)
        {
            switch (id)
            {
                case 0: return CustomerTaxStatus.Export;
                case 1: return CustomerTaxStatus.NonTaxable;
                case 2: return CustomerTaxStatus.Taxable;
                default: return CustomerTaxStatus.Taxable;
            }

        }


        /// <summary>
        /// Gets Product Id by it's SKU
        /// </summary>
        /// <param name="pSKU">SKU of Product</param>
        /// <returns>Product ID</returns>
        public static int GetProductIdFromSKU(string pSKU)
        {
            int productId = 0;
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetProductsIDBySKU, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SKU, pSKU));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        productId = Convert.ToInt32(_cmd.ExecuteScalar());
                        ////= cmd.Parameters["@id"].Value.ToString();
                        //lblMessage.Text = "Record inserted successfully. ID = " + id;
                        //using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        //{
                        //    List<ProductModel> products = new List<ProductModel>();
                        //    DataTable oDataTable = new DataTable();
                        //    a.Fill(oDataTable);
                        //    products = Utility.DataReaderMapToList<ProductModel>(oDataTable);

                        //}
                    }
                }

            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }

            return productId;
        }

        /// <summary>
        /// Gets Description of Product by it's SKU
        /// </summary>
        /// <param name="pSKU">SKU of product</param>
        /// <returns>Description of Product</returns>
        public static string GetProductDescriptionBySKU(string pSKU)
        {
            string ProductDesc = string.Empty;
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.getProductDescriptionBySKU, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SKU, pSKU));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        ProductDesc = Convert.ToString(_cmd.ExecuteScalar());
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return ProductDesc;
        }

    }
}

