﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Web;

namespace AT_BusinessLayer.AT_Model
{
    [DataContract]
    class SalesRepresentativeModel
    {
        private int _SalesRepresentativeId;
        private String _Code;
        private String _Name;
        private Boolean _IsActive;
        private int _CommissionMethodId;
        private String _Street;
        private String _Suburb;
        private String _State;
        private String _PostCode;
        private String _Country;
        private String _Telephone1;
        private String _Telephone2;
        private String _Fax;
        private String _Email;
        private String _CreatedBy;
        private DateTime _CreatedOn;
        private String _LastModifiedBy;
        private DateTime _LastModifiedOn;
        private DateTime _RecordStamp;

       
    }
}
