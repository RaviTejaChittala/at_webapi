﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Web;


namespace AT.Sybiz.Library
{
    [DataContract]
    public class ProductModel
    {
        private int _ProductId;
        private string _Code;
        private string _Description;
        private string _ExtendedDescription;
        private Boolean _IsActive;
        private Boolean _IsDiminishingStock;
        private Boolean _AllowNegativeStock;
        private Boolean _IsSerialTracked;
        private Boolean _IsLotTracked;
        private Boolean _AllowDuplicateSerialNo;
        private Boolean _IsLotUseByDateReqd;
        private Boolean _AllowTrackingOnStockIn;
        private Boolean _AllowTrackingOnStockOut;
        private int _SortCodeId;
        private int _ProductGroupId;
        private int _CostingMethodId;
        private float _MassValue;
        private int _MassUnitId;
        private float _VolumeValue;
        private float _VolumeUnitId;
        private string _Remarks;
        private decimal _LastCost;
        private decimal _StandardCost;
        private int _SalesTaxCodeId;
        private int _PurchasesTaxCodeId;
        private Byte[] _Photo;
        private string _Website;
        private string _DocumentFolder;
        private string _Notes;
        private string _SalesNotes;
        private int _StructuredProductId;
        private Boolean _EnforceCodeStructure;
        private Boolean _EnforceDescriptionStructure;
        private Boolean _EnforceExtendedDescriptionStructure;
        private Boolean _SettingsManagedByProduct;
        private string _CreatedBy;
        private DateTime _CreatedOn;
        private string _LastModifiedBy;
        private DateTime _LastModifiedOn;
        private Byte[] _RecordStamp;
        private string _Swatch_color;
        private string _Featured;
        private string _Trending;
        private int _Moq;
        private string _Personality;
        private string _Preorder_note;
        private string _Preorder_cart_label;
        private string _Use_smd_colorswatch;
        private string _Instagram_source;
        private string _Instagram_source_user;
        private string _Design;
        private string _Name;
        private int _Qty;
        private string _Type;
        private Boolean _Has_options;
        private string _Visibility;
        private string _Tax_class_id;
        private Boolean _Is_in_stock;
        private string _Associated;
        private string _Config_attributes;
        private int _ProductBarcodeId;
        private string _Barcode;
        private int _LocationId;
        private int _UnitOfMeasureId;
        //private int _PriceScaleId;
        private Decimal _RetailPrice;
        private Decimal _WholeSalePrice;
        private Decimal _OverseasPrice;
        private Decimal _TradePrice;


        [DataMember(Name = "ProductId")]
        public int ProductId
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        [DataMember(Name = "Code")]
        public String Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [DataMember(Name = "Description")]
        public String Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember(Name = "ExtendedDescription")]
        public String ExtendedDescription
        {
            get { return _ExtendedDescription; }
            set { _ExtendedDescription = value; }
        }

        [DataMember(Name = "IsActive")]
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        [DataMember(Name = "IsDiminishingStock")]
        public bool IsDiminishingStock
        {
            get { return _IsDiminishingStock; }
            set { _IsDiminishingStock = value; }
        }

        [DataMember(Name = "AllowNegativeStock")]
        public bool AllowNegativeStock
        {
            get { return _AllowNegativeStock; }
            set { _AllowNegativeStock = value; }
        }

        [DataMember(Name = "IsSerialTracked")]
        public bool IsSerialTracked
        {
            get { return _IsSerialTracked; }
            set { _IsSerialTracked = value; }
        }

        [DataMember(Name = "IsLotTracked")]
        public bool IsLotTracked
        {
            get { return _IsLotTracked; }
            set { _IsLotTracked = value; }
        }

        [DataMember(Name = "AllowDuplicateSerialNo")]
        public bool AllowDuplicateSerialNo
        {
            get { return _AllowDuplicateSerialNo; }
            set { _AllowDuplicateSerialNo = value; }
        }

        [DataMember(Name = "IsLotUseByDateReqd")]
        public bool IsLotUseByDateReqd
        {
            get { return _IsLotUseByDateReqd; }
            set { _IsLotUseByDateReqd = value; }
        }

        [DataMember(Name = "AllowTrackingOnStockIn")]
        public bool AllowTrackingOnStockIn
        {
            get { return _AllowTrackingOnStockIn; }
            set { _AllowTrackingOnStockIn = value; }
        }

        [DataMember(Name = "AllowTrackingOnStockOut")]
        public bool AllowTrackingOnStockOut
        {
            get { return _AllowTrackingOnStockOut; }
            set { _AllowTrackingOnStockOut = value; }
        }

        [DataMember(Name = "SortCodeId")]
        public int SortCodeId
        {
            get { return _SortCodeId; }
            set { _SortCodeId = value; }
        }

        [DataMember(Name = "ProductGroupId")]
        public int ProductGroupId
        {
            get { return _ProductGroupId; }
            set { _ProductGroupId = value; }
        }

        [DataMember(Name = "CostingMethodId")]
        public int CostingMethodId
        {
            get { return _CostingMethodId; }
            set { _CostingMethodId = value; }
        }

        [DataMember(Name = "MassValue")]
        public Double MassValue
        {
            get;
            set;
        }

        [DataMember(Name = "MassUnitId")]
        public int MassUnitId
        {
            get { return _MassUnitId; }
            set { _MassUnitId = value; }
        }

        [DataMember(Name = "VolumeValue")]
        public Double VolumeValue
        {
            get;
            set;
        }

        [DataMember(Name = "VolumeUnitId")]
        public int VolumeUnitId
        {
            get;
            set;
        }

        [DataMember(Name = "Remarks")]
        public String Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        [DataMember(Name = "LastCost")]
        public Decimal LastCost
        {
            get { return _LastCost; }
            set { _LastCost = value; }
        }

        [DataMember(Name = "StandardCost")]
        public Decimal StandardCost
        {
            get { return _StandardCost; }
            set { _StandardCost = value; }
        }

        [DataMember(Name = "SalesTaxCodeId")]
        public int SalesTaxCodeId
        {
            get { return _SalesTaxCodeId; }
            set { _SalesTaxCodeId = value; }
        }

        [DataMember(Name = "PurchasesTaxCodeId")]
        public int PurchasesTaxCodeId
        {
            get { return _PurchasesTaxCodeId; }
            set { _PurchasesTaxCodeId = value; }
        }

        [DataMember(Name = "Photo")]
        public Byte[] Photo
        {
            get { return _Photo; }
            set { _Photo = value; }
        }

        [DataMember(Name = "Website")]
        public String Website
        {
            get { return _Website; }
            set { _Website = value; }
        }

        [DataMember(Name = "DocumentFolder")]
        public String DocumentFolder
        {
            get { return _DocumentFolder; }
            set { _DocumentFolder = value; }
        }

        [DataMember(Name = "Notes")]
        public String Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        [DataMember(Name = "SalesNotes")]
        public String SalesNotes
        {
            get { return _SalesNotes; }
            set { _SalesNotes = value; }
        }

        [DataMember(Name = "StructuredProductId")]
        public int StructuredProductId
        {
            get { return _StructuredProductId; }
            set { _StructuredProductId = value; }
        }

        [DataMember(Name = "EnforceCodeStructure")]
        public bool EnforceCodeStructure
        {
            get { return _EnforceCodeStructure; }
            set { _EnforceCodeStructure = value; }
        }

        [DataMember(Name = "EnforceDescriptionStructure")]
        public bool EnforceDescriptionStructure
        {
            get { return _EnforceDescriptionStructure; }
            set { _EnforceDescriptionStructure = value; }
        }

        [DataMember(Name = "EnforceExtendedDescriptionStructure")]
        public bool EnforceExtendedDescriptionStructure
        {
            get { return _EnforceExtendedDescriptionStructure; }
            set { _EnforceExtendedDescriptionStructure = value; }
        }

        [DataMember(Name = "SettingsManagedByProduct")]
        public bool SettingsManagedByProduct
        {
            get { return _SettingsManagedByProduct; }
            set { _SettingsManagedByProduct = value; }
        }

        [DataMember(Name = "CreatedBy")]
        public String CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        [DataMember(Name = "CreatedOn")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }

        [DataMember(Name = "LastModifiedBy")]
        public String LastModifiedBy
        {
            get { return _LastModifiedBy; }
            set { _LastModifiedBy = value; }
        }

        [DataMember(Name = "LastModifiedOn")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { _LastModifiedOn = value; }
        }

        [DataMember(Name = "RecordStamp")]
        public Byte[] RecordStamp
        {
            get { return _RecordStamp; }
            set { _RecordStamp = value; }
        }

        [DataMember(Name = "Swatch_color")]
        public String Swatch_color
        {
            get { return _Swatch_color; }
            set { _Swatch_color = value; }
        }

        [DataMember(Name = "Featured")]
        public String Featured
        {
            get { return _Featured; }
            set { _Featured = value; }
        }

        [DataMember(Name = "Trending")]
        public String Trending
        {
            get { return _Trending; }
            set { _Trending = value; }
        }

        [DataMember(Name = "Moq")]
        public int Moq
        {
            get { return _Moq; }
            set { _Moq = value; }
        }

        [DataMember(Name = "Personality")]
        public String Personality
        {
            get { return _Personality; }
            set { _Personality = value; }
        }

        [DataMember(Name = "Preorder_note")]
        public String Preorder_note
        {
            get { return _Preorder_note; }
            set { _Preorder_note = value; }
        }

        [DataMember(Name = "Preorder_cart_label")]
        public String Preorder_cart_label
        {
            get { return _Preorder_cart_label; }
            set { _Preorder_cart_label = value; }
        }

        [DataMember(Name = "Use_smd_colorswatch")]
        public String Use_smd_colorswatch
        {
            get { return _Use_smd_colorswatch; }
            set { _Use_smd_colorswatch = value; }
        }

        [DataMember(Name = "Instagram_source")]
        public String Instagram_source
        {
            get { return _Instagram_source; }
            set { _Instagram_source = value; }
        }

        [DataMember(Name = "Instagram_source_user")]
        public String Instagram_source_user
        {
            get { return _Instagram_source_user; }
            set { _Instagram_source_user = value; }
        }

        [DataMember(Name = "Design")]
        public String Design
        {
            get { return _Design; }
            set { _Design = value; }
        }

        [DataMember(Name = "Name")]
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [DataMember(Name = "Qty")]
        public int Qty
        {
            get { return _Qty; }
            set { _Qty = value; }
        }

        [DataMember(Name = "Type")]
        public String Type
        {
            get { return _Type; }
            set { _Type = value; }
        }

        [DataMember(Name = "Has_options")]
        public bool Has_options
        {
            get { return _Has_options; }
            set { _Has_options = value; }
        }

        [DataMember(Name = "Visibility")]
        public String Visibility
        {
            get { return _Visibility; }
            set { _Visibility = value; }
        }

        [DataMember(Name = "Tax_class_id")]
        public String Tax_class_id
        {
            get { return _Tax_class_id; }
            set { _Tax_class_id = value; }
        }

        [DataMember(Name = "Is_in_stock")]
        public bool Is_in_stock
        {
            get { return _Is_in_stock; }
            set { _Is_in_stock = value; }
        }

        [DataMember(Name = "Associated")]
        public String Associated
        {
            get { return _Associated; }
            set { _Associated = value; }
        }

        [DataMember(Name = "Config_attributes")]
        public String Config_attributes
        {
            get { return _Config_attributes; }
            set { _Config_attributes = value; }
        }

        [DataMember(Name = "ProductBarcodeId")]
        public int ProductBarcodeId
        {
            get { return _ProductBarcodeId; }
            set { _ProductBarcodeId = value; }
        }

        [DataMember(Name = "Barcode")]
        public String Barcode
        {
            get { return _Barcode; }
            set { _Barcode = value; }
        }

        [DataMember(Name = "LocationId")]
        public int LocationId
        {
            get { return _LocationId; }
            set { _LocationId = value; }
        }

        [DataMember(Name = "UnitOfMeasureId")]
        public int UnitOfMeasureId
        {
            get { return _UnitOfMeasureId; }
            set { _UnitOfMeasureId = value; }
        }

        //[DataMember(Name = "PriceScaleId")]
        //public int PriceScaleId
        //{
        //    get { return _PriceScaleId; }
        //    set { _PriceScaleId = value; }
        //}

        [DataMember(Name = "RetailPrice")]
        public Decimal RetailPrice
        {
            get { return _RetailPrice; }
            set { _RetailPrice = value; }
        }

        [DataMember(Name = "WholeSalePrice")]
        public Decimal WholeSalePrice
        {
            get { return _WholeSalePrice; }
            set { _WholeSalePrice = value; }
        }

        [DataMember(Name = "TradePrice")]
        public Decimal TradePrice
        {
            get { return _TradePrice; }
            set { _TradePrice = value; }
        }

        [DataMember(Name = "OverseasPrice")]
        public Decimal OverseasPrice
        {
            get { return _OverseasPrice; }
            set { _OverseasPrice = value; }
        }
    }
}
