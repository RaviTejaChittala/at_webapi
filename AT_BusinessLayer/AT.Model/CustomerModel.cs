﻿using Newtonsoft.Json;
using Sybiz.Vision.Platform.Debtors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AT_BusinessLayer.AT.Model
{
    [DataContract]
    public class DeliveryAddress1
    {
        private int _CustomerId;
        private int _AddressTypeId;
        private string _Street;
        private string _Suburb;
        private string _State;
        private string _PostCode;
        private string _Country;
        private Boolean _IsPrimary;
        private string _DeliveryCode;

        [DataMember(Name = "CustomerId")]
        public int CustomerId
        {
            get { return _CustomerId; }
            set { _CustomerId = value; }
        }

        [DataMember(Name = "AddressTypeId")]
        public int AddressTypeId
        {
            get { return _AddressTypeId; }
            set { _AddressTypeId = value; }
        }

        [DataMember(Name = "Street")]
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }

        [DataMember(Name = "Suburb")]
        public string Suburb
        {
            get { return _Suburb; }
            set { _Suburb = value; }
        }

        [DataMember(Name = "State")]
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        [DataMember(Name = "Country")]
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        [DataMember(Name = "PostCode")]
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        [DataMember(Name = "IsPrimary")]
        public Boolean IsPrimary
        {
            get { return _IsPrimary; }
            set { _IsPrimary = value; }
        }

        [DataMember(Name = "DeliveryCode")]
        public string DeliveryCode
        {
            get { return _DeliveryCode; }
            set { _DeliveryCode = value; }
        }
    }

    [DataContract]
    public class PhysicalAddress
    {
        private string _Street;
        private string _Suburb;
        private string _State;
        private string _PostCode;
        private string _Country;


        [DataMember(Name = "Street")]
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }

        [DataMember(Name = "Suburb")]
        public string Suburb
        {
            get { return _Suburb; }
            set { _Suburb = value; }
        }

        [DataMember(Name = "State")]
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        [DataMember(Name = "Country")]
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        [DataMember(Name = "PostCode")]
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }
    }

    [DataContract]
    public class PostalAddress
    {
        private string _Street;
        private string _Suburb;
        private string _State;
        private string _PostCode;
        private string _Country;


        [DataMember(Name = "Street")]
        public string Street
        {
            get { return _Street; }
            set { _Street = value; }
        }

        [DataMember(Name = "Suburb")]
        public string Suburb
        {
            get { return _Suburb; }
            set { _Suburb = value; }
        }

        [DataMember(Name = "State")]
        public string State
        {
            get { return _State; }
            set { _State = value; }
        }

        [DataMember(Name = "Country")]
        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        [DataMember(Name = "PostCode")]
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }
    }

    [DataContract]
    public class CustomerModel
    {
        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        private int _CustomerId;
        private string _Code;
        private string _Name;
        private string _TradingName;
        private int _SortCodeId;
        private bool _IsActive;
        private bool _IsProspective;
        private int _GroupId;
        private decimal _CreditLimit;
        private decimal _InterestRate;
        private string _TaxNumber;
        private int _PriceScaleId;
        private int _TradingTermId;
        private int _AccountingMethodId;
        private int _AnalysisCodeId;
        private int _TaxStatusId;
        private int _TaxCodeId;
        private decimal _PromptPaymentDiscount;
        private string _PromptPaymentText;
        private int _WithholdingMethodId;
        private decimal _WithholdingRate;
        private decimal _WithholdingMinimumThreshold;
        private decimal _WithholdingMaximumThreshold;
        private Boolean _VoluntaryUsed;
        private Decimal _VoluntaryRate;
        private Decimal _VoluntaryMinimumThreshold;
        private Decimal _VoluntaryMaximumThreshold;
        private string _Remarks;
        private string _OutlookFolder;
        private string _DocumentFolder;
        private string _DeliveryInstructions;
        private string _WebSite;
        private string _Notes;
        private int _TerritoryId;
        private int _IndustryId;
        private string _Telephone1;
        private string _Telephone2;
        private string _Fax;
        private string _Email;
        private Boolean _ReferenceNumberRequired;
        private int _SalesRepresentativeId;
        private int _SalesLocationId;
        private bool _StopOrders;
        private bool _StopDeliveries;
        private bool _StopInvoices;
        private bool _StopJobs;
        private int _HeadOfficeId;
        private string _CreditHoldReason;
        private bool _DeliveryAccountRequired;
        private string _CreatedBy;
        private string _LastModifiedBy;

        ////private string _PhysicalCountry;
        //private string _PhysicalPostCode;
        //private string _PhysicalState;
        ////private string _PhysicalStreet;
        //private string _PhysicalSuburb;

        //private string _PostalCountry;
        //private string _PostalPostCode;
        //private string _PostalState;
        //private string _PostalStreet;
        //private string _PostalSuburb;

        //[DataMember(Name = "PhysicalCountry")]
        //public string PhysicalCountry
        //{
        //    get { return _PhysicalCountry; }
        //    set { _PhysicalCountry = value; }
        //}

        //[DataMember(Name = "PhysicalPostCode")]
        //public string PhysicalPostCode
        //{
        //    get { return _PhysicalPostCode; }
        //    set { _PhysicalPostCode = value; }
        //}

        //[DataMember(Name = "PhysicalState")]
        //public string PhysicalState
        //{
        //    get { return _PhysicalState; }
        //    set { _PhysicalState = value; }
        //}

        //[DataMember(Name = "PhysicalStreet")]
        //public string PhysicalStreet
        //{
        //    get { return _PhysicalStreet; }
        //    set { _PhysicalStreet = value; }
        //}

        //[DataMember(Name = "PhysicalSuburb")]
        //public string PhysicalSuburb
        //{
        //    get { return _PhysicalSuburb; }
        //    set { _PhysicalSuburb = value; }
        //}

        //[DataMember(Name = "PostalCountry")]
        //public string PostalCountry
        //{
        //    get { return _PostalCountry; }
        //    set { _PostalCountry = value; }
        //}

        //[DataMember(Name = "PostalPostCode")]
        //public string PostalPostCode
        //{
        //    get { return _PostalPostCode; }
        //    set { _PostalPostCode = value; }
        //}

        //[DataMember(Name = "PostalState")]
        //public string PostalState
        //{
        //    get { return _PostalState; }
        //    set { _PostalState = value; }
        //}

        //[DataMember(Name = "PostalStreet")]
        //public string PostalStreet
        //{
        //    get { return _PostalStreet; }
        //    set { _PostalStreet = value; }
        //}

        //[DataMember(Name = "PostalSuburb")]
        //public string PostalSuburb
        //{
        //    get { return _PostalSuburb; }
        //    set { _PostalSuburb = value; }
        //}

        [DataMember(Name = "CustomerId")]
        public int CustomerId
        {
            get { return _CustomerId; }
            set { _CustomerId = value; }
        }

        [DataMember(Name = "Code")]
        public string Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [DataMember(Name = "Name")]
        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [DataMember(Name = "TradingName")]
        public string TradingName
        {
            get { return _TradingName; }
            set { _TradingName = value; }
        }

        [DataMember(Name = "SortCodeId")]
        public int SortCodeId
        {
            get { return _SortCodeId; }
            set { _SortCodeId = value; }
        }

        [DataMember(Name = "IsActive")]
        public Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        [DataMember(Name = "IsProspective")]
        public Boolean IsProspective
        {
            get { return _IsProspective; }
            set { _IsProspective = value; }
        }

        [DataMember(Name = "GroupId")]
        public int GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        [DataMember(Name = "CreditLimit")]
        public decimal CreditLimit
        {
            get { return _CreditLimit; }
            set { _CreditLimit = value; }
        }

        [DataMember(Name = "InterestRate")]
        public decimal InterestRate
        {
            get { return _InterestRate; }
            set { _InterestRate = value; }
        }

        [DataMember(Name = "TaxNumber")]
        public string TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }

        [DataMember(Name = "PriceScaleId")]
        public int PriceScaleId
        {
            get { return _PriceScaleId; }
            set { _PriceScaleId = value; }
        }

        [DataMember(Name = "TradingTermId")]
        public int TradingTermId
        {
            get { return _TradingTermId; }
            set { _TradingTermId = value; }
        }

        [DataMember(Name = "AccountingMethodId")]
        public int AccountingMethodId
        {
            get { return _AccountingMethodId; }
            set { _AccountingMethodId = value; }
        }

        [DataMember(Name = "AnalysisCodeId")]
        public int AnalysisCodeId
        {
            get { return _AnalysisCodeId; }
            set { _AnalysisCodeId = value; }
        }

        [DataMember(Name = "TaxStatusId")]
        public int TaxStatusId
        {
            get { return _TaxStatusId; }
            set { _TaxStatusId = value; }
        }

        [DataMember(Name = "TaxCodeId")]
        public int TaxCodeId
        {
            get { return _TaxCodeId; }
            set { _TaxCodeId = value; }
        }

        [DataMember(Name = "PromptPaymentDiscount")]
        public decimal PromptPaymentDiscount
        {
            get { return _PromptPaymentDiscount; }
            set { _PromptPaymentDiscount = value; }
        }

        [DataMember(Name = "PromptPaymentText")]
        public string PromptPaymentText
        {
            get { return _PromptPaymentText; }
            set { _PromptPaymentText = value; }
        }

        [DataMember(Name = "WithholdingMethodId")]
        public int WithholdingMethodId
        {
            get { return _WithholdingMethodId; }
            set { _WithholdingMethodId = value; }
        }

        [DataMember(Name = "WithholdingRate")]
        public decimal WithholdingRate
        {
            get { return _WithholdingRate; }
            set { _WithholdingRate = value; }
        }

        [DataMember(Name = "WithholdingMinimumThreshold")]
        public decimal WithholdingMinimumThreshold
        {
            get { return _WithholdingMinimumThreshold; }
            set { _WithholdingMinimumThreshold = value; }
        }

        [DataMember(Name = "WithholdingMaximumThreshold")]
        public decimal WithholdingMaximumThreshold
        {
            get { return _WithholdingMaximumThreshold; }
            set { _WithholdingMaximumThreshold = value; }
        }

        [DataMember(Name = "VoluntaryUsed")]
        public Boolean VoluntaryUsed
        {
            get { return _VoluntaryUsed; }
            set { _VoluntaryUsed = value; }
        }

        [DataMember(Name = "VoluntaryRate")]
        public decimal VoluntaryRate
        {
            get { return _VoluntaryRate; }
            set { _VoluntaryRate = value; }
        }

        [DataMember(Name = "VoluntaryMinimumThreshold")]
        public decimal VoluntaryMinimumThreshold
        {
            get { return _VoluntaryMinimumThreshold; }
            set { _VoluntaryMinimumThreshold = value; }
        }

        [DataMember(Name = "VoluntaryMaximumThreshold")]
        public decimal VoluntaryMaximumThreshold
        {
            get { return _VoluntaryMaximumThreshold; }
            set { _VoluntaryMaximumThreshold = value; }
        }

        [DataMember(Name = "Remarks")]
        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        [DataMember(Name = "OutlookFolder")]
        public string OutlookFolder
        {
            get { return _OutlookFolder; }
            set { _OutlookFolder = value; }
        }

        [DataMember(Name = "DocumentFolder")]
        public string DocumentFolder
        {
            get { return _DocumentFolder; }
            set { _DocumentFolder = value; }
        }

        [DataMember(Name = "DeliveryInstructions")]
        public string DeliveryInstructions
        {
            get { return _DeliveryInstructions; }
            set { _DeliveryInstructions = value; }
        }

        [DataMember(Name = "WebSite")]
        public string WebSite
        {
            get { return _WebSite; }
            set { _WebSite = value; }
        }

        [DataMember(Name = "TerritoryId")]
        public int TerritoryId
        {
            get { return _TerritoryId; }
            set { _TerritoryId = value; }
        }

        [DataMember(Name = "IndustryId")]
        public int IndustryId
        {
            get { return _IndustryId; }
            set { _IndustryId = value; }
        }

        [DataMember(Name = "Telephone1")]
        public string Telephone1
        {
            get { return _Telephone1; }
            set { _Telephone1 = value; }
        }

        [DataMember(Name = "Telephone2")]
        public string Telephone2
        {
            get { return _Telephone2; }
            set { _Telephone2 = value; }
        }

        [DataMember(Name = "Fax")]
        public string Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        [DataMember(Name = "Email")]
        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        [DataMember(Name = "ReferenceNumberRequired")]
        public Boolean ReferenceNumberRequired
        {
            get { return _ReferenceNumberRequired; }
            set { _ReferenceNumberRequired = value; }
        }

        [DataMember(Name = "SalesRepresentativeId")]
        public int SalesRepresentativeId
        {
            get { return _SalesRepresentativeId; }
            set { _SalesRepresentativeId = value; }
        }

        [DataMember(Name = "SalesLocationId")]
        public int SalesLocationId
        {
            get { return _SalesLocationId; }
            set { _SalesLocationId = value; }
        }

        [DataMember(Name = "StopOrders")]
        public Boolean StopOrders
        {
            get { return _StopOrders; }
            set { _StopOrders = value; }
        }

        [DataMember(Name = "StopDeliveries")]
        public Boolean StopDeliveries
        {
            get { return _StopDeliveries; }
            set { _StopDeliveries = value; }
        }

        [DataMember(Name = "StopInvoices")]
        public Boolean StopInvoices
        {
            get { return _StopInvoices; }
            set { _StopInvoices = value; }
        }

        [DataMember(Name = "StopJobs")]
        public Boolean StopJobs
        {
            get { return _StopJobs; }
            set { _StopJobs = value; }
        }

        [DataMember(Name = "HeadOfficeId")]
        public int HeadOfficeId
        {
            get { return _HeadOfficeId; }
            set { _HeadOfficeId = value; }
        }

        [DataMember(Name = "CreditHoldReason")]
        public string CreditHoldReason
        {
            get { return _CreditHoldReason; }
            set { _CreditHoldReason = value; }
        }

        [DataMember(Name = "DeliveryAccountRequired")]
        public Boolean DeliveryAccountRequired
        {
            get { return _DeliveryAccountRequired; }
            set { _DeliveryAccountRequired = value; }
        }

        [DataMember(Name = "CreatedBy")]
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        [DataMember(Name = "LastModifiedBy")]
        public string LastModifiedBy
        {
            get { return _LastModifiedBy; }
            set { _LastModifiedBy = value; }
        }

        [DataMember(Name = "Notes")]
        public string Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        public CustomerModel() { }

        [DataMember(Name = "Delivery")]
        public List<DeliveryAddress1> Delivery
        {
            get;
            set;
        }

        [DataMember(Name = "Physical")]
        public PhysicalAddress Physical
        {
            get;
            set;
        }

        [DataMember(Name = "Postal")]
        public PostalAddress Postal
        {
            get;
            set;
        }
    }
}
