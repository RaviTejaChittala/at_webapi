﻿using Sybiz.Vision.Platform.Debtors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AT_BusinessLayer.AT.Model
{
    [DataContract]
    public class SupplierModel
    {
        private int _SupplierId;
        private String _Code;
        private String _Name;
        private String _TradingName;
        private int _SortCodeId;
        private Boolean _IsActive;
        private int _GroupId;
        private Decimal _CreditLimit;
        private String _TaxNumber;
        private String _Remarks;
        private int _TradingTermId;
        private int _AccountingMethodId;
        private Decimal _PromptPaymentDiscount;
        private String _PromptPaymentText;
        private int _WithholdingMethodId;
        private Decimal _WithholdingRate;
        private Decimal _WithholdingMinimumThreshold;
        private Decimal _WithholdingMaximumThreshold;
        private Boolean _VoluntaryUsed;
        private Decimal _VoluntaryRate;
        private Decimal _VoluntaryMinimumThreshold;
        private Decimal _VoluntaryMaximumThreshold;
        private String _OutlookFolder;
        private String _DocumentFolder;
        private String _Website;
        private String _Notes;
        private String _Telephone1;
        private String _Telephone2;
        private String _Fax;
        private String _Email;
        private int _AnalysisCodeId;
        private int _PurchaseLocationId;
        private Boolean _ReferenceNumberRequired;
        private String _CreatedBy;
        private DateTime _CreatedOn;
        private String _LastModifiedBy;
        private DateTime _LastModifiedOn;
        private Byte[] _RecordStamp;
        private int _TaxStatusId;
        private int _TaxCodeId;
        private String _DeliveryInstructions;
        private Boolean _BlockInvoicePayments;

        public string PhysicalCountry { get; set; }
        public string PhysicalPostCode { get; set; }
        public string PhysicalState { get; set; }
        public string PhysicalStreet { get; set; }
        public string PhysicalSuburb { get; set; }

        public string PostalCountry { get; set; }
        public string PostalPostCode { get; set; }
        public string PostalState { get; set; }
        public string PostalStreet { get; set; }
        public string PostalSuburb { get; set; }





        [DataMember(Name = "SupplierId")]
        public int SupplierId
        {
            get { return _SupplierId; }
            set { _SupplierId = value; }
        }

        [DataMember(Name = "Code")]
        public String Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [DataMember(Name = "Name")]
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [DataMember(Name = "TradingName")]
        public String TradingName
        {
            get { return _TradingName; }
            set { _TradingName = value; }
        }

        [DataMember(Name = "SortCodeId")]
        public int SortCodeId
        {
            get { return _SortCodeId; }
            set { _SortCodeId = value; }
        }

        [DataMember(Name = "IsActive")]
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        [DataMember(Name = "GroupId")]
        public int GroupId
        {
            get { return _GroupId; }
            set { _GroupId = value; }
        }

        [DataMember(Name = "CreditLimit")]
        public decimal CreditLimit
        {
            get { return _CreditLimit; }
            set { _CreditLimit = value; }
        }

        [DataMember(Name = "TaxNumber")]
        public String TaxNumber
        {
            get { return _TaxNumber; }
            set { _TaxNumber = value; }
        }

        [DataMember(Name = "Remarks")]
        public String Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        [DataMember(Name = "TradingTermId")]
        public int TradingTermId
        {
            get { return _TradingTermId; }
            set { _TradingTermId = value; }
        }

        [DataMember(Name = "AccountingMethodId")]
        public int AccountingMethodId
        {
            get { return _AccountingMethodId; }
            set { _AccountingMethodId = value; }
        }

        [DataMember(Name = "PromptPaymentDiscount")]
        public Decimal PromptPaymentDiscount
        {
            get { return _PromptPaymentDiscount; }
            set { _PromptPaymentDiscount = value; }
        }

        [DataMember(Name = "PromptPaymentText")]
        public String PromptPaymentText
        {
            get { return _PromptPaymentText; }
            set { _PromptPaymentText = value; }
        }

        [DataMember(Name = "WithholdingMethodId")]
        public int WithholdingMethodId
        {
            get { return _WithholdingMethodId; }
            set { _WithholdingMethodId = value; }
        }

        [DataMember(Name = "WithholdingRate")]
        public Decimal WithholdingRate
        {
            get { return _WithholdingRate; }
            set { _WithholdingRate = value; }
        }

        [DataMember(Name = "WithholdingMinimumThreshold")]
        public Decimal WithholdingMinimumThreshold
        {
            get { return _WithholdingMinimumThreshold; }
            set { _WithholdingMinimumThreshold = value; }
        }

        [DataMember(Name = "WithholdingMaximumThreshold")]
        public Decimal WithholdingMaximumThreshold
        {
            get { return _WithholdingMaximumThreshold; }
            set { _WithholdingMaximumThreshold = value; }
        }

        [DataMember(Name = "VoluntaryUsed")]
        public Boolean VoluntaryUsed
        {
            get { return _VoluntaryUsed; }
            set { _VoluntaryUsed = value; }
        }

        [DataMember(Name = "VoluntaryRate")]
        public Decimal VoluntaryRate
        {
            get { return _VoluntaryRate; }
            set { _VoluntaryRate = value; }
        }

        [DataMember(Name = "VoluntaryMinimumThreshold")]
        public Decimal VoluntaryMinimumThreshold
        {
            get { return _VoluntaryMinimumThreshold; }
            set { _VoluntaryMinimumThreshold = value; }
        }

        [DataMember(Name = "VoluntaryMaximumThreshold")]
        public Decimal VoluntaryMaximumThreshold
        {
            get { return _VoluntaryMaximumThreshold; }
            set { _VoluntaryMaximumThreshold = value; }
        }

        [DataMember(Name = "OutlookFolder")]
        public String OutlookFolder
        {
            get { return _OutlookFolder; }
            set { _OutlookFolder = value; }
        }

        [DataMember(Name = "DocumentFolder")]
        public String DocumentFolder
        {
            get { return _DocumentFolder; }
            set { _DocumentFolder = value; }
        }

        [DataMember(Name = "Website")]
        public String Website
        {
            get { return _Website; }
            set { _Website = value; }
        }

        [DataMember(Name = "Notes")]
        public String Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }

        [DataMember(Name = "Telephone1")]
        public String Telephone1
        {
            get { return _Telephone1; }
            set { _Telephone1 = value; }
        }

        [DataMember(Name = "Telephone2")]
        public String Telephone2
        {
            get { return _Telephone2; }
            set { _Telephone2 = value; }
        }

        [DataMember(Name = "Fax")]
        public String Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }
        [DataMember(Name = "Email")]
        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        [DataMember(Name = "AnalysisCodeId")]
        public int AnalysisCodeId
        {
            get { return _AnalysisCodeId; }
            set { _AnalysisCodeId = value; }
        }

        [DataMember(Name = "PurchaseLocationId")]
        public int PurchaseLocationId
        {
            get { return _PurchaseLocationId; }
            set { _PurchaseLocationId = value; }
        }

        [DataMember(Name = "ReferenceNumberRequired")]
        public Boolean ReferenceNumberRequired
        {
            get { return _ReferenceNumberRequired; }
            set { _ReferenceNumberRequired = value; }
        }

        [DataMember(Name = "CreatedBy")]
        public String CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        [DataMember(Name = "CreatedOn")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }

        [DataMember(Name = "LastModifiedBy")]
        public String LastModifiedBy
        {
            get { return _LastModifiedBy; }
            set { _LastModifiedBy = value; }
        }

        [DataMember(Name = "LastModifiedOn")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { _LastModifiedOn = value; }
        }

        [DataMember(Name = "RecordStamp")]
        public Byte[] RecordStamp
        {
            get { return _RecordStamp; }
            set { _RecordStamp = value; }
        }

        [DataMember(Name = "TaxStatusId")]
        public int TaxStatusId
        {
            get { return _TaxStatusId; }
            set { _TaxStatusId = value; }
        }

        [DataMember(Name = "DeliveryInstructions")]
        public String DeliveryInstructions
        {
            get { return _DeliveryInstructions; }
            set { _DeliveryInstructions = value; }
        }

        [DataMember(Name = "BlockInvoicePayments")]
        public Boolean BlockInvoicePayments
        {
            get { return _BlockInvoicePayments; }
            set { _BlockInvoicePayments = value; }
        }

        [DataMember(Name = "TaxCodeId")]
        public int TaxCodeId
        {
            get { return _TaxCodeId; }
            set { _TaxCodeId = value; }
        }
        public SupplierModel()
        { }
    }
}
