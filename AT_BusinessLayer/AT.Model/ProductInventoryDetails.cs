﻿using AT.Sybiz.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace AT_BusinessLayer.AT.Model
{
    [DataContract]
    public class ProductQuantitiesToUpdate
    {
        private int _ProductId;
        private string _Code;
        private string _Description;
        private string _ExtendedDescription;
        private Boolean _IsActive;
        private Decimal _Available;

        [DataMember(Name = "ProductId")]
        public int ProductId
        {
            get { return _ProductId; }
            set { _ProductId = value; }
        }

        [DataMember(Name = "Code")]
        public String Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [DataMember(Name = "Description")]
        public String Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        [DataMember(Name = "ExtendedDescription")]
        public String ExtendedDescription
        {
            get { return _ExtendedDescription; }
            set { _ExtendedDescription = value; }
        }

        [DataMember(Name = "IsActive")]
        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        [DataMember(Name = "Available")]
        public Decimal Available
        {
            get { return _Available; }
            set { _Available = value; }
        }

    }
}
