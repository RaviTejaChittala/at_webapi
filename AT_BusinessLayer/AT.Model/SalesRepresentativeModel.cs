﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Web;

namespace AT.Sybiz.Library.AT.Model
{
    [DataContract]
    public class SalesRepresentativeModel
    {
        private int _SalesRepresentativeId;
        private String _Code;
        private String _Name;
        private Boolean _IsActive;
        private int _CommissionMethodId;
        private String _Street;
        private String _Suburb;
        private String _State;
        private String _PostCode;
        private String _Country;
        private String _Telephone1;
        private String _Telephone2;
        private String _Fax;
        private String _Email;
        private String _CreatedBy;
        private DateTime _CreatedOn;
        private String _LastModifiedBy;
        private DateTime _LastModifiedOn;
        private DateTime _RecordStamp;
        private Boolean _HasGlobalAccess;
        private DateTime? _GlobalAccessEndDate;        

        [DataMember(Name = "SalesRepresentativeId")]
        public int SalesRepresentativeId
        {
            get { return _SalesRepresentativeId; }
            set { _SalesRepresentativeId = value; }
        }

        [DataMember(Name = "Code")]
        public String Code
        {
            get { return _Code; }
            set { _Code = value; }
        }

        [DataMember(Name = "Name")]
        public String Name
        {
            get { return _Name; }
            set { _Name = value; }
        }

        [DataMember(Name = "IsActive")]
        public Boolean IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }

        [DataMember(Name = "CommissionMethodId")]
        public int CommissionMethodId
        {
            get { return _CommissionMethodId; }
            set { _CommissionMethodId = value; }
        }

        [DataMember(Name = "Street")]
        public String Street
        {
            get { return _Street; }
            set { _Street = value; }
        }

        [DataMember(Name = "Suburb")]
        public String Suburb
        {
            get { return _Suburb; }
            set { _Suburb = value; }
        }

        [DataMember(Name = "State")]
        public String State
        {
            get { return _State; }
            set { _State = value; }
        }

        [DataMember(Name = "PostCode")]
        public String PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }

        [DataMember(Name = "Country")]
        public String Country
        {
            get { return _Country; }
            set { _Country = value; }
        }

        [DataMember(Name = "Telephone1")]
        public String Telephone1
        {
            get { return _Telephone1; }
            set { _Telephone1 = value; }
        }

        [DataMember(Name = "Telephone2")]
        public String Telephone2
        {
            get { return _Telephone2; }
            set { _Telephone2 = value; }
        }

        [DataMember(Name = "Fax")]
        public String Fax
        {
            get { return _Fax; }
            set { _Fax = value; }
        }

        [DataMember(Name = "Email")]
        public String Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        [DataMember(Name = "CreatedBy")]
        public String CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; }
        }

        [DataMember(Name = "CreatedOn")]
        public DateTime CreatedOn
        {
            get { return _CreatedOn; }
            set { _CreatedOn = value; }
        }

        [DataMember(Name = "LastModifiedBy")]
        public String LastModifiedBy
        {
            get { return _LastModifiedBy; }
            set { _LastModifiedBy = value; }
        }

        [DataMember(Name = "LastModifiedOn")]
        public DateTime LastModifiedOn
        {
            get { return _LastModifiedOn; }
            set { _LastModifiedOn = value; }
        }

        [DataMember(Name = "HasGlobalAccess")]
        public Boolean HasGlobalAccess
        {
            get { return _HasGlobalAccess; }
            set { _HasGlobalAccess = value; }
        }

        [DataMember(Name = "GlobalAccessEndDate")]
        public DateTime? GlobalAccessEndDate
        {
            get { return _GlobalAccessEndDate; }
            set { _GlobalAccessEndDate = value; }
        } 
    }
}
