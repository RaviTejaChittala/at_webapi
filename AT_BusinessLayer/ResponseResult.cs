﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace AT_BusinessLayer
{
    public class ResponseResult
    {
        public string Message { get; set; }

        public string Data { get; set; }

        public HttpStatusCode Status { get; set; }

        public ResponseResult()
        {
        }

    }
}
