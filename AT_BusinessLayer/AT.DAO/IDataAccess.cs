﻿using AT.Sybiz.Library.AT.Model;
using AT_BusinessLayer;
using AT_BusinessLayer.AT.Model;
using Sybiz.Vision.Platform.Creditors;
using Sybiz.Vision.Platform.Debtors;
using Sybiz.Vision.Platform.Debtors.Transaction;
using Sybiz.Vision.Platform.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AT.Sybiz.Library.AT.DAO
{
    public interface IDataAccess
    {

        #region "Products"

        List<ProductModel> GetAllProducts();

        List<ProductModel> GetAllProductsWithinLimit(int start, int limit);

        ProductModel GetProductInfoBySKU(string sku);

        List<ProductModel> GetModifiedProductsFromDate(string lastSyncDate);

        List<ProductModel> GetCreatedProductsFromDate(string lastSyncDate);

        List<ProductModel> GetDeletedProductsFromDate(string lastSyncDate);

        ProductGroupInfoList GetProductGroup();

        List<ProductQuantitiesToUpdate> GetInventoryDetailAnalytic();
        #endregion

        #region "Sales Representative"

        //SalesRepresentativeInfoList GetSalesRepresentative();
        List<SalesRepresentativeModel> GetSalesRepresentative();

        List<SalesRepresentativeModel> GetCreatedSalesRepresentativesFromDate(string lastSyncDate);

        List<SalesRepresentativeModel> GetDeletedSalesRepresentativesFromDate(string lastSyncDate);

        ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj);

        //ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj);

        //ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id);

        ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id);

        #endregion

        #region "Customer"

        //Customer GetCustomers();

        List<CustomerModel> GetCreatedCustomersFromDate(string lastSyncDate);

        ResponseResult SetDeliveryAddressAsPrimary(int custId, int addressId);

        List<CustomerModel> GetDeletedCustomersFromDate(string lastSyncDate);

        ResponseResult CreateCustomer(CustomerModel CObj);

        ResponseResult UpdateCustomer(CustomerModel CObj, int id);

        ResponseResult InsertCustomerAddress(DeliveryAddress1 DAObj);

        #endregion

        #region "Supplier"

        SupplierInfoList GetSupplier();

        List<SupplierModel> GetCreatedSuppliersFromDate(string lastSyncDate);

        List<SupplierModel> GetDeletedSuppliersFromDate(string lastSyncDate);

        ResponseResult CreateSupplier(SupplierModel SObj);

        #endregion

        //void CreateSalesOrder(SalesOrderModel SOObj);
        ResponseResult CreateSalesOrder(SalesOrderModel SOObj);
        //ProductModel GetProduct();
      
        //SupplierInfoList get();

        //List<CustomerModel> GetModifiedCustomersFromDate(string lastSyncDate);

        //SalesOrder GetSalesOrder();
    }
}
