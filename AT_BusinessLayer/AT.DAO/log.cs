﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AT_BusinessLayer.AT.DAO
{
    class log
    {
        public static void WriteErrorLogs(Exception ex)
        {
            try
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile.txt", true);
                sw.Write(DateTime.Now.ToString() + ": " + ex + Environment.NewLine);
                sw.Flush();
                sw.Close();
            }
            catch
            {

            }
        }
        public static void WriteErrorLogs(string ex)
        {
            try
            {
                StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile.txt", true);
                sw.Write(DateTime.Now.ToString() + ": " + ex + Environment.NewLine);
                sw.Flush();
                sw.Close();
            }
            catch
            {

            }
        }
    }
}
