﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
//using Sybiz.Vision.Platform.Inventory;
//using Sybiz.Vision.Platform.Security;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using Sybiz.Vision.Platform.Debtors;
using Sybiz.Vision.Platform.Security;
using Sybiz.Vision.Platform.Inventory;
using Sybiz.Vision.Platform.Debtors.Transaction;
using System.Reflection;
using AT_BusinessLayer;
using Sybiz.Vision.Platform.Creditors;
using Csla.Server;
using AT.Sybiz.Library.AT.Model;
using AT_BusinessLayer.AT.Model;
using Sybiz.Vision.Platform.Core.Enumerations;
using System.Net;
using Sybiz.Vision.Platform.Core.Transaction;
using Sybiz.Vision.Platform.Common;
using AT_BusinessLayer.AT.DAO;


namespace AT.Sybiz.Library.AT.DAO
{
    public class ATDAO : IDataAccess
    {

        //private List<ProductModel> ProductModel = new List<ProductModel>();
        string connectionString = ConfigurationManager.ConnectionStrings["SQLConnectionString"].ConnectionString;


        //public ProductModel GetProduct()
        //{
        //    //Principal.LogIn("Administrator", "Admin", connectionString);
        //    //return ProductInfoList.GetList();
        //    //string connectionString = WebConfigurationManager.AppSettings["SQLConnectionString"];


        //    ProductModel prod = new ProductModel();

        //    using (SqlConnection _con = new SqlConnection(connectionString))
        //    {
        //        using (SqlCommand _cmd = new SqlCommand("getProductData", _con))
        //        {
        //            _cmd.CommandType = CommandType.StoredProcedure;
        //            _con.Open();
        //            _cmd.CommandTimeout = 1000;
        //            using (SqlDataReader dr = _cmd.ExecuteReader())
        //            {
        //                if (dr.Read() == true)
        //                {
        //                    prod.ProductId = Convert.ToInt32(dr["ProductId"]);
        //                    prod.Code = dr["Code"].ToString();
        //                    prod.Description = dr["Description"].ToString();
        //                    prod.ExtendedDescription = dr["ExtendedDescription"].ToString();
        //                    prod.IsActive = Convert.ToBoolean(dr["IsActive"]);
        //                    prod.IsDiminishingStock = Convert.ToBoolean(dr["IsDiminishingStock"]);
        //                }
        //            }
        //        }
        //    }
        //    return prod;
        //}

        #region "Product"

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<ProductModel> GetAllProducts()
        {
            List<ProductModel> products = new List<ProductModel>();

            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {

                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetAllProducts, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _con.Open();
                        _cmd.CommandTimeout = 1000;

                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            products = Utility.DataReaderMapToList<ProductModel>(oDataTable);
                        }
                    }
                }
                log.WriteErrorLogs("Products reterieved successfully!");
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return products;

        }

        /// <summary>
        /// Get all Products within a specified limit
        /// </summary>
        /// <returns></returns>
        public List<ProductModel> GetAllProductsWithinLimit(int start, int limit)
        {
            List<ProductModel> products = new List<ProductModel>();

            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {

                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetAllProductsWithinLimit, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.Start, start));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.Limit, limit));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;

                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            products = Utility.DataReaderMapToList<ProductModel>(oDataTable);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return products;

        }


        /// <summary>
        /// Get Product Groups
        /// </summary>
        /// <returns></returns>
        public ProductGroupInfoList GetProductGroup()
        {
            try
            {
                Utility.IsLoginToSybiz();
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }

            return ProductGroupInfoList.GetList();
        }

        /// <summary>
        /// Get products by SKU
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public ProductModel GetProductInfoBySKU(string sku)
        {
            ProductModel product = new ProductModel();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetProductsBySKU, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SKU, sku));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            List<ProductModel> products = new List<ProductModel>();
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            products = Utility.DataReaderMapToList<ProductModel>(oDataTable);
                            product = products[0];
                        }
                    }
                }

            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return product;

        }

        /// <summary>
        /// Get modified products after a specific date
        /// </summary>
        /// <param name="lastSyncDate"></param>
        /// <returns></returns>
        public List<ProductModel> GetModifiedProductsFromDate(string lastSyncDate)
        {
            List<ProductModel> products = new List<ProductModel>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetModifiedProducts, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataReader dr = _cmd.ExecuteReader())
                        {
                            products = Utility.DataReaderMapToList<ProductModel>(dr);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return products;
        }


        public List<ProductModel> GetCreatedProductsFromDate(string lastSyncDate)
        {
            List<ProductModel> products = new List<ProductModel>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetCreatedProducts, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            products = Utility.DataReaderMapToList<ProductModel>(oDataTable);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return products;
        }

        public List<ProductModel> GetDeletedProductsFromDate(string lastSyncDate)
        {
            List<ProductModel> products = new List<ProductModel>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.GetDeletedProducts, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            products = Utility.DataReaderMapToList<ProductModel>(oDataTable);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return products;
        }

        public List<ProductQuantitiesToUpdate> GetInventoryDetailAnalytic()
        {
            List<ProductQuantitiesToUpdate> products = new List<ProductQuantitiesToUpdate>();

            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {

                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.ProductQuantitiesToUpdate, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _con.Open();
                        _cmd.CommandTimeout = 1000;

                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            products = Utility.DataReaderMapToList<ProductQuantitiesToUpdate>(oDataTable);
                        }
                    }
                }
                log.WriteErrorLogs("Product Details to Update reterieved successfully!");
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return products;

        }

        #endregion

        #region "Sales Representative"

        /// <summary>
        /// Get sales representatives
        /// </summary>
        /// <returns></returns>
        //public SalesRepresentativeInfoList GetSalesRepresentative()
        //{
        //    //Principal.LogIn("Administrator", "Admin", connectionString);
        //    Utility.IsLoginToSybiz();
        //    return SalesRepresentativeInfoList.GetList();

        //}

        public List<SalesRepresentativeModel> GetSalesRepresentative()
        {
            List<SalesRepresentativeModel> salesRep = new List<SalesRepresentativeModel>();

            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {

                    using (SqlCommand _cmd = new SqlCommand(ATConstants.SalesRepresentative.StoredProcedure.GetAllSalesRepresentatives, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _con.Open();
                        _cmd.CommandTimeout = 1000;

                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            salesRep = Utility.DataReaderMapToList<SalesRepresentativeModel>(oDataTable);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.WriteErrorLogs(ex);
            }
            return salesRep;

        }

        public List<SalesRepresentativeModel> GetCreatedSalesRepresentativesFromDate(string lastSyncDate)
        {
            List<SalesRepresentativeModel> salesReps = new List<SalesRepresentativeModel>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.SalesRepresentative.StoredProcedure.GetCreatedSalesRepresentatives, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            salesReps = Utility.DataReaderMapToList<SalesRepresentativeModel>(oDataTable);
                        }

                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return salesReps;

        }

        public List<SalesRepresentativeModel> GetDeletedSalesRepresentativesFromDate(string lastSyncDate)
        {
            List<SalesRepresentativeModel> salesReps = new List<SalesRepresentativeModel>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.SalesRepresentative.StoredProcedure.GetDeletedSalesRepresentatives, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            salesReps = Utility.DataReaderMapToList<SalesRepresentativeModel>(oDataTable);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return salesReps;
        }

        //public ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj)
        //{
        //    ResponseResult result = new ResponseResult();
        //    if (Utility.IsLoginToSybiz())
        //    {
        //        SalesRepresentative oSalesRep = SalesRepresentative.GetObject(0);
        //        oSalesRep.Code = SRObj.Code;// "test123";
        //        oSalesRep.Name = SRObj.Name;// "Daffodil Test";
        //        oSalesRep.IsActive = SRObj.IsActive;// false;
        //        //oSalesRep=SRObj.CommissionMethodId
        //        oSalesRep.Email = SRObj.Email;// "abc@gmail.com";
        //        oSalesRep.Fax = SRObj.Fax;// string.Empty;                
        //        oSalesRep.Telephone1 = SRObj.Telephone1;
        //        oSalesRep.Telephone2 = SRObj.Telephone2;
        //        oSalesRep.Address.Country = SRObj.Country;
        //        oSalesRep.Address.PostCode = SRObj.PostCode;
        //        oSalesRep.Address.State = SRObj.State;
        //        oSalesRep.Address.Street = SRObj.Street;
        //        oSalesRep.Address.Suburb = SRObj.Suburb;
        //        //oSalesRep. = SRObj.Suburb;    


        //        if (oSalesRep.BrokenRulesCollection.Count > 0)
        //        {
        //            StringBuilder errorString = new StringBuilder();
        //            result.Status = HttpStatusCode.ExpectationFailed;
        //            foreach (var brokenRule in oSalesRep.BrokenRulesCollection)
        //            {
        //                errorString.Append(brokenRule.Description);
        //                errorString.AppendLine();
        //                result.Message += brokenRule.RuleName + "\n";
        //            }
        //            result.Message = errorString.ToString();
        //            result.Data = string.Empty;
        //        }
        //        else
        //        {
        //            oSalesRep = oSalesRep.Save();
        //            result.Status = HttpStatusCode.OK;
        //            result.Data = oSalesRep.Id.ToString();
        //            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
        //        }
        //    }
        //    else
        //    {
        //        result.Status = HttpStatusCode.Unauthorized;
        //        result.Message = "Login failed";//.Id.ToString();
        //        result.Data = string.Empty;
        //    }
        //    Principal.LogOut();
        //    return result;
        //}

        public ResponseResult CreateSalesRepresentative(SalesRepresentativeModel SRObj)
        {
            ResponseResult result = new ResponseResult();
            Utility.IsLoginToSybiz();
            SqlConnection _con = null;
            try
            {
                using (_con = new SqlConnection(Utility.GetConnectionString()))
                {

                    using (SqlCommand _cmd = new SqlCommand(ATConstants.SalesRepresentative.StoredProcedure.InsertSalesRepresentative, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Code, SRObj.Code));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Name, SRObj.Name));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.IsActive, SRObj.IsActive));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.CommissionMethodId, SRObj.CommissionMethodId));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Country, SRObj.Country));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.CreatedBy, SRObj.CreatedBy));
                        //_cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.CommissionMethodId, obj.CreatedOn));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Email, SRObj.Email));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Fax, SRObj.Fax));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.LastModifiedBy, SRObj.LastModifiedBy));
                        //_cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.LastModifiedOn, obj.LastModifiedOn));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.PostCode, SRObj.PostCode));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.State, SRObj.State));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Street, SRObj.Street));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Suburb, SRObj.Suburb));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Telephone1, SRObj.Telephone1));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Telephone2, SRObj.Telephone2));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.HasGlobalAccess, SRObj.HasGlobalAccess));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.GlobalAccessEndDate, SRObj.GlobalAccessEndDate));
                        // _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Id, ParameterDirection.Output));

                        int nid;
                        _con.Open();
                        //_cmd.CommandTimeout = 1000;
                        //if(!Is)
                        //var rowsAffected = (int)_cmd.ExecuteScalar();
                        SqlDataReader reader = _cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            //string id = _cmd.Parameters["@id"].Value.ToString(); 

                            //if (reader.HasRows)
                            //{
                            reader.Read();
                            nid = Convert.ToInt32(reader[0]);
                            //MessageBox.Show("ID : " + nid);

                            result.Status = HttpStatusCode.OK;
                            result.Data = nid.ToString();
                            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                            //_con.Close();
                            //}
                        }
                        else
                        {
                            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Failure;
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                log.WriteErrorLogs(ex);
                //Console.Write("ERROR:" + ex.Message);
            }
            finally
            {
                if (_con != null && _con.State != ConnectionState.Closed)
                {
                    _con.Close();
                }
            }

            return result;
        }

        //public ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id)
        //{
        //    ResponseResult result = new ResponseResult();
        //    if (Utility.IsLoginToSybiz())
        //    {
        //        SalesRepresentative oSalesRep = SalesRepresentative.GetObject(id);
        //        oSalesRep.Code = SRObj.Code;// "test123";
        //        oSalesRep.Name = SRObj.Name;// "Daffodil Test";
        //        oSalesRep.IsActive = SRObj.IsActive;// false;
        //        //oSalesRep=SRObj.CommissionMethodId
        //        oSalesRep.Email = SRObj.Email;// "abc@gmail.com";
        //        oSalesRep.Fax = SRObj.Fax;// string.Empty;                
        //        oSalesRep.Telephone1 = SRObj.Telephone1;
        //        oSalesRep.Telephone2 = SRObj.Telephone2;
        //        //oSalesRep.
        //        oSalesRep.Address.Country = SRObj.Country;
        //        oSalesRep.Address.PostCode = SRObj.PostCode;
        //        oSalesRep.Address.State = SRObj.State;
        //        oSalesRep.Address.Street = SRObj.Street;
        //        oSalesRep.Address.Suburb = SRObj.Suburb;               
        //        //oSalesRep. = SRObj.Suburb;    


        //        if (oSalesRep.BrokenRulesCollection.Count > 0)
        //        {
        //            StringBuilder errorString = new StringBuilder();
        //            result.Status = HttpStatusCode.ExpectationFailed;
        //            foreach (var brokenRule in oSalesRep.BrokenRulesCollection)
        //            {
        //                errorString.Append(brokenRule.Description);
        //                errorString.AppendLine();
        //                result.Message += brokenRule.RuleName + "\n";
        //            }
        //            result.Message = errorString.ToString();
        //            result.Data = string.Empty;
        //        }
        //        else
        //        {
        //            oSalesRep = oSalesRep.Save();
        //            result.Status = HttpStatusCode.OK;
        //            result.Data = oSalesRep.Id.ToString();
        //            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
        //        }
        //    }
        //    else
        //    {
        //        result.Status = HttpStatusCode.Unauthorized;
        //        result.Message = "Login failed";//.Id.ToString();
        //        result.Data = string.Empty;
        //    }
        //    Principal.LogOut();
        //    return result;
        //}

        public ResponseResult UpdateSalesRepresentative(SalesRepresentativeModel SRObj, int id)
        {
            //Utility.IsLoginToSybiz();
            ResponseResult result = new ResponseResult();
            using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
            {
                try
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.SalesRepresentative.StoredProcedure.UpdateSalesRepresentative, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Id, id));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Code, SRObj.Code));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Name, SRObj.Name));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.IsActive, SRObj.IsActive));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.CommissionMethodId, SRObj.CommissionMethodId));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Country, SRObj.Country));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.CreatedBy, SRObj.CreatedBy));
                        //_cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.CommissionMethodId, obj.CreatedOn));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Email, SRObj.Email));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Fax, SRObj.Fax));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.LastModifiedBy, SRObj.LastModifiedBy));
                        //_cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.LastModifiedOn, obj.LastModifiedOn));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.PostCode, SRObj.PostCode));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.State, SRObj.State));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Street, SRObj.Street));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Suburb, SRObj.Suburb));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Telephone1, SRObj.Telephone1));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.Telephone2, SRObj.Telephone2));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.HasGlobalAccess, SRObj.HasGlobalAccess));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.SalesRepresentative.StoredProcedure.Parameters.GlobalAccessEndDate, SRObj.GlobalAccessEndDate));

                        //int nid;
                        _con.Open();
                        //_cmd.CommandTimeout = 1000;
                        //
                        int rowCount = _cmd.ExecuteNonQuery();
                        //SqlDataReader reader = _cmd.ExecuteReader();
                        if (rowCount != 0)
                        {
                            //string id = _cmd.Parameters["@id"].Value.ToString(); 

                            result.Status = HttpStatusCode.OK;
                            result.Data = id.ToString();
                            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                            //_con.Close();
                            //}
                        }
                        //_con.Close();
                    }
                }
                catch (Exception ex)
                {
                    result.Message = ex.Message;
                    log.WriteErrorLogs(ex);
                }
                finally
                {
                    if (_con != null && _con.State != ConnectionState.Closed)
                    {
                        _con.Close();
                    }
                }

                return result;
            }
        }

        #endregion

        #region "Customer"

        //Customer GetCustomers();

        public List<CustomerModel> GetCreatedCustomersFromDate(string lastSyncDate)
        {

            List<CustomerModel> customers = new List<CustomerModel>();

            //if (Utility.IsLoginToSybiz())
            //{
            //    var customers1 = CustomerInfoList.GetList().Take(10);//.Where(p => p filteredTags.All(tag => p.Tags.Contains(tag)));
            //    List<CustomerDetailInfo> customeDetailInfoList = new List<CustomerDetailInfo>();

            //    customers1.Cast<CustomerInfo>().ToList().ForEach(cust =>
            //    {
            //        customeDetailInfoList.Add(CustomerDetailInfo.GetObject(cust.Id));
            //    });
            //}
            // List<CustomerInfo> customers1 = new List<CustomerInfo>();
            //var customers1 = CustomerInfoList.GetList().Where((cust, index) => Convert.ToDateTime(cust.ExtendedProperties.GetCustomField("CreateOn")) <= Convert.ToDateTime(lastSyncDate));
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Customer.StoredProcedure.GetCreatedCustomers, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);

                            customers = Utility.DataReaderMapToList<CustomerModel>(oDataTable);
                        }
                    }
                }
                customers.ForEach(cust =>
                {
                    var customerAddresses = GetCustomerAddressByCustID(cust.CustomerId);
                    var postalAddress = (from customer in customerAddresses.Cast<DeliveryAddress1>()
                                         where customer.AddressTypeId == 2
                                         select customer).ToList().FirstOrDefault();
                    cust.Postal = new PostalAddress
                    {
                        Street = postalAddress.Street,
                        Country = postalAddress.Country,
                        State = postalAddress.State,
                        Suburb = postalAddress.Suburb,
                        PostCode = postalAddress.PostCode
                    };
                    var physicalAddress = (from customer in customerAddresses.Cast<DeliveryAddress1>()
                                           where customer.AddressTypeId == 1
                                           select customer).ToList().FirstOrDefault();
                    cust.Physical = new PhysicalAddress
                    {
                        Street = physicalAddress.Street,
                        Country = physicalAddress.Country,
                        State = physicalAddress.Street,
                        Suburb = physicalAddress.Suburb,
                        PostCode = physicalAddress.PostCode
                    };

                    var deliveryAddress = (from customer in customerAddresses.Cast<DeliveryAddress1>()
                                           where customer.AddressTypeId == 3
                                           select customer).ToList();

                    cust.Delivery = deliveryAddress;
                });

            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            //return 
            return customers;
        }

        private List<DeliveryAddress1> GetCustomerAddressByCustID(int custID)
        {
            List<DeliveryAddress1> deliveryAddress = new List<DeliveryAddress1>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Customer.StoredProcedure.GetCustomerAddressByCustID, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.CustomerID, custID));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            deliveryAddress = Utility.DataReaderMapToList<DeliveryAddress1>(oDataTable);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return deliveryAddress;
        }

        public ResponseResult SetDeliveryAddressAsPrimary(int custId, int addressId)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Customer.StoredProcedure.SetDeliveryAddressAsPrimary, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.AddressID, addressId));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.CustomerID, custId));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        if (_cmd.ExecuteNonQuery() > 0)
                        {
                            result.Status = HttpStatusCode.OK;
                            result.Message = "Address set as primary";//.Id.ToString();
                            result.Data = string.Empty;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return result;
        }

        public List<CustomerModel> GetDeletedCustomersFromDate(string lastSyncDate)
        {
            List<CustomerModel> customers = new List<CustomerModel>();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.Customer.StoredProcedure.GetDeletedCustomers, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                        _con.Open();
                        _cmd.CommandTimeout = 1000;
                        using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                        {
                            DataTable oDataTable = new DataTable();
                            a.Fill(oDataTable);
                            customers = Utility.DataReaderMapToList<CustomerModel>(oDataTable);
                        }
                    }
                }

                customers.ForEach(cust =>
                {
                    var customerAddresses = GetCustomerAddressByCustID(cust.CustomerId);
                    var postalAddress = (from customer in customerAddresses.Cast<DeliveryAddress1>()
                                         where customer.AddressTypeId == 2
                                         select customer).ToList().FirstOrDefault();
                    cust.Postal = new PostalAddress
                    {
                        Street = postalAddress.Street,
                        Country = postalAddress.Country,
                        State = postalAddress.State,
                        Suburb = postalAddress.Suburb,
                        PostCode = postalAddress.PostCode
                    };
                    var physicalAddress = (from customer in customerAddresses.Cast<DeliveryAddress1>()
                                           where customer.AddressTypeId == 1
                                           select customer).ToList().FirstOrDefault();
                    cust.Physical = new PhysicalAddress
                    {
                        Street = physicalAddress.Street,
                        Country = physicalAddress.Country,
                        State = physicalAddress.Street,
                        Suburb = physicalAddress.Suburb,
                        PostCode = physicalAddress.PostCode
                    };

                    var deliveryAddress = (from customer in customerAddresses.Cast<DeliveryAddress1>()
                                           where customer.AddressTypeId == 3
                                           select customer).ToList();

                    cust.Delivery = deliveryAddress;
                });
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return customers;
        }

        public ResponseResult CreateCustomer(CustomerModel CObj)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                if (Utility.IsLoginToSybiz())
                {
                    Customer oCustomer = Customer.GetObject(0);
                    oCustomer.Code = CObj.Code;// "test123";
                    oCustomer.Email = CObj.Email;// "abc@gmail.com";
                    oCustomer.Fax = CObj.Fax;// string.Empty;
                    oCustomer.GroupId = CObj.GroupId;// 1;
                    oCustomer.IsActive = CObj.IsActive;// false;
                    oCustomer.Name = CObj.Name;// "Daffodil Test";
                    oCustomer.SortCodeId = CObj.SortCodeId;// 76;
                    oCustomer.TradingName = CObj.TradingName;// "abc";
                    oCustomer.TradingTermsId = CObj.TradingTermId;// 2;
                    oCustomer.WebSite = CObj.WebSite;// string.Empty;
                    oCustomer.IsProspective = CObj.IsProspective; //Utility.GetSupplierTaxStatus(CObj.TaxStatusId);
                    oCustomer.TaxStatus = Utility.GetCustomerTaxStatus(CObj.TaxStatusId);
                    //oCustomer.TaxStatus = Utility.GetCustomerTaxStatus(2);
                    oCustomer.InterestRate = CObj.InterestRate;// 0.00;
                    oCustomer.CreditLimit = CObj.CreditLimit;// 100;
                    oCustomer.Telephone1 = CObj.Telephone1;
                    oCustomer.Telephone2 = CObj.Telephone2;
                    oCustomer.TaxNumber = CObj.TaxNumber;
                    oCustomer.Remarks = CObj.Remarks;

                    //Customer Physical Address
                    oCustomer.PhysicalAddress.Country = CObj.Physical.Country;
                    oCustomer.PhysicalAddress.PostCode = CObj.Physical.PostCode;
                    oCustomer.PhysicalAddress.State = CObj.Physical.State;
                    oCustomer.PhysicalAddress.Street = CObj.Physical.Street;
                    oCustomer.PhysicalAddress.Suburb = CObj.Physical.Suburb;

                    //Customer Postal Address
                    oCustomer.PostalAddress.Country = CObj.Postal.Country;
                    oCustomer.PostalAddress.PostCode = CObj.Postal.PostCode;
                    oCustomer.PostalAddress.State = CObj.Postal.State;
                    oCustomer.PostalAddress.Street = CObj.Postal.Street;
                    oCustomer.PostalAddress.Suburb = CObj.Postal.Suburb;

                    //DeliveryAddressList address = new DeliveryAddressList();
                    //DeliveryAddress addres = DeliveryAddress.GetObject(0);
                    //foreach (DeliveryAddress1 a in CObj.Delivery)
                    //{                        
                    //    addres.Street = a.Street;
                    //    addres.Suburb = a.Suburb;
                    //    addres.State = a.State;
                    //    addres.Country = a.Country;
                    //    addres.PostCode = a.PostCode;
                    //    addres.PrimaryAddress = a.IsPrimary;
                    //}                    
                    //address.Add(addres);
                    //oCustomer.DeliveryAddress = address;

                    oCustomer.PromptPaymentText = CObj.Telephone2;
                    oCustomer.PriceScale = CObj.PriceScaleId;

                    // oCustomer.AccountingMethod = AccountingMethod.OpenItem();// CObj.AccountingMethodId;
                    oCustomer.TaxCodeId = CObj.TaxCodeId;

                    // oCustomer.WithholdingAmountWithheldMinimumThreshold = CObj.WithholdingMinimumThreshold;// "55 926 289 191";
                    oCustomer.Remarks = CObj.Remarks;// "TEstINg";
                    oCustomer.DeliveryInstructions = CObj.DeliveryInstructions;
                    oCustomer.PromptPaymentDiscount = CObj.PromptPaymentDiscount;// "test payment";
                    //oCustomer.WithholdingAmountWithheldMinimumThreshold = CObj.WithholdingMinimumThreshold;// .10;
                    oCustomer.OutlookFolder = CObj.OutlookFolder;// "outlook";
                    oCustomer.DocumentFolder = CObj.DocumentFolder;// "outlook";
                    oCustomer.AnalysisCodeId = CObj.AnalysisCodeId;
                    oCustomer.SalesLocationId = CObj.SalesLocationId;
                    // oCustomer.SalesRepresentativeId = CObj.SalesRepresentativeId;
                    // oCustomer.WithholdingAmountWithheldMethod = Utility.GetWithholdingAmountWithheld(CObj.WithholdingMethodId); //WithholdingAmountWithheldMethod.Always;
                    if (oCustomer.BrokenRulesCollection.Count > 0)
                    {
                        StringBuilder errorString = new StringBuilder();
                        result.Status = HttpStatusCode.ExpectationFailed;
                        foreach (var brokenRule in oCustomer.BrokenRulesCollection)
                        {
                            errorString.Append(brokenRule.Description);
                            errorString.AppendLine();
                            // result.Message += brokenRule.RuleName + "\n";
                        }
                        result.Message = errorString.ToString();
                        result.Data = string.Empty;
                        log.WriteErrorLogs(result.Message);
                    }
                    else
                    {
                        oCustomer = oCustomer.Save();
                        result.Status = HttpStatusCode.OK;
                        result.Data = oCustomer.Id.ToString();
                        result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                        log.WriteErrorLogs("Login Successful + Customer Created");
                    }
                }
                else
                {
                    result.Status = HttpStatusCode.Unauthorized;
                    result.Message = "Login failed";//.Id.ToString();
                    result.Data = string.Empty;
                    log.WriteErrorLogs("Login Failed");
                }
                Principal.LogOut();

            }

            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return result;

            //using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
            //{
            //    using (SqlCommand _cmd = new SqlCommand(ATConstants.Customer.StoredProcedure.InsertCustomer, _con))
            //    {
            //        _cmd.CommandType = CommandType.StoredProcedure;
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Code, CObj.Code));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Name, CObj.Name));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.IsActive, CObj.IsActive));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.AccountingMethodId, CObj.AccountingMethodId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.AnalysisCodeId, CObj.AnalysisCodeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.CreatedBy, CObj.CreatedBy));
            //        //_cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.CommissionMethodId, obj.CreatedOn));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Email, CObj.Email));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Fax, CObj.Fax));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.LastModifiedBy, CObj.LastModifiedBy));
            //        //_cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.LastModifiedOn, obj.LastModifiedOn));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.CreditHoldReason, CObj.CreditHoldReason));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.CreditLimit, CObj.CreditLimit));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.DeliveryAccountRequired, CObj.DeliveryAccountRequired));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.DeliveryInstructions, CObj.DeliveryInstructions));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Telephone1, CObj.Telephone1));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Telephone2, CObj.Telephone2));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.GroupId, CObj.GroupId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.HeadOfficeId, CObj.HeadOfficeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.IndustryId, CObj.IndustryId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.InterestRate, CObj.InterestRate));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.IsProspective, CObj.IsProspective));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Notes, CObj.Notes));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.OutlookFolder, CObj.OutlookFolder));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.PriceScaleId, CObj.PriceScaleId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.PromptPaymentDiscount, CObj.PromptPaymentDiscount));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.PromptPaymentText, CObj.PromptPaymentText));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.ReferenceNumberRequired, CObj.ReferenceNumberRequired));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Remarks, CObj.Remarks));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.SalesLocationId, CObj.SalesLocationId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.SalesRepresentativeId, CObj.SalesRepresentativeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.SortCodeId, CObj.SortCodeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.StopDeliveries, CObj.StopDeliveries));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.StopInvoices, CObj.StopInvoices));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.StopJobs, CObj.StopJobs));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.StopOrders, CObj.StopOrders));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.TaxCodeId, CObj.TaxCodeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.TaxNumber, CObj.TaxNumber));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.TaxStatusId, CObj.TaxStatusId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.TerritoryId, CObj.TerritoryId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.TradingName, CObj.TradingName));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.TradingTermId, CObj.TradingTermId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.VoluntaryMaximumThreshold, CObj.VoluntaryMaximumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.VoluntaryMinimumThreshold, CObj.VoluntaryMinimumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.VoluntaryRate, CObj.VoluntaryRate));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.VoluntaryUsed, CObj.VoluntaryUsed));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.Website, CObj.WebSite));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.WithholdingMaximumThreshold, CObj.WithholdingMaximumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.WithholdingMethodId, CObj.WithholdingMethodId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.WithholdingMinimumThreshold, CObj.WithholdingMinimumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Customer.StoredProcedure.Parameters.WithholdingRate, CObj.WithholdingRate));


            //        _con.Open();
            //        _cmd.CommandTimeout = 1000;
            //        _cmd.ExecuteNonQuery();
            //        _con.Close();
            //    }
            //}

        }

        public ResponseResult UpdateCustomer(CustomerModel CObj, int id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                if (Utility.IsLoginToSybiz())
                {
                    Customer oCustomer = Customer.GetObject(id);

                    oCustomer.Code = CObj.Code;// "test123";
                    oCustomer.Email = CObj.Email;// "abc@gmail.com";
                    oCustomer.Fax = CObj.Fax;// string.Empty;
                    oCustomer.GroupId = CObj.GroupId;// 1;
                    oCustomer.IsActive = CObj.IsActive;// false;
                    oCustomer.Name = CObj.Name;// "Daffodil Test";
                    oCustomer.SortCodeId = CObj.SortCodeId;// 76;
                    oCustomer.TradingName = CObj.TradingName;// "abc";
                    oCustomer.TradingTermsId = CObj.TradingTermId;// 2;
                    oCustomer.WebSite = CObj.WebSite;// string.Empty;
                    //oCustomer.IsProspective = CObj.IsProspective; //Utility.GetSupplierTaxStatus(CObj.TaxStatusId);
                    oCustomer.TaxStatus = Utility.GetCustomerTaxStatus(CObj.TaxStatusId);
                    oCustomer.InterestRate = CObj.InterestRate;// 0.00;
                    oCustomer.CreditLimit = CObj.CreditLimit;// 100;
                    oCustomer.Telephone1 = CObj.Telephone1;
                    oCustomer.Telephone2 = CObj.Telephone2;
                    oCustomer.TaxNumber = CObj.TaxNumber;
                    oCustomer.Remarks = CObj.Remarks;

                    //Customer Physical Address
                    oCustomer.PhysicalAddress.Country = CObj.Physical.Country;
                    oCustomer.PhysicalAddress.PostCode = CObj.Physical.PostCode;
                    oCustomer.PhysicalAddress.State = CObj.Physical.State;
                    oCustomer.PhysicalAddress.Street = CObj.Physical.Street;
                    oCustomer.PhysicalAddress.Suburb = CObj.Physical.Suburb;

                    //Customer Postal Address
                    oCustomer.PostalAddress.Country = CObj.Postal.Country;
                    oCustomer.PostalAddress.PostCode = CObj.Postal.PostCode;
                    oCustomer.PostalAddress.State = CObj.Postal.State;
                    oCustomer.PostalAddress.Street = CObj.Postal.Street;
                    oCustomer.PostalAddress.Suburb = CObj.Postal.Suburb;

                    //DeliveryAddressList address = new DeliveryAddressList();
                    //DeliveryAddress addres = DeliveryAddress.GetObject(0);

                    ////var a = Convert.ChangeType(CObj.Delivery, typeof(DeliveryAddress1));

                    //foreach (DeliveryAddress1 a in CObj.Delivery)
                    //{
                    //    addres.Street = a.Street;
                    //    addres.Suburb = a.Suburb;
                    //    addres.State = a.State;
                    //    addres.Country = a.Country;
                    //    addres.PostCode = a.PostCode;
                    //    addres.PrimaryAddress = a.IsPrimary;
                    //}

                    //address.Add(addres);
                    //oCustomer.DeliveryAddress = address;

                    oCustomer.PromptPaymentText = CObj.Telephone2;
                    oCustomer.PriceScale = CObj.PriceScaleId;

                    // oCustomer.AccountingMethod = AccountingMethod.OpenItem();// CObj.AccountingMethodId;
                    oCustomer.TaxCodeId = CObj.TaxCodeId;

                    // oCustomer.WithholdingAmountWithheldMinimumThreshold = CObj.WithholdingMinimumThreshold;// "55 926 289 191";
                    oCustomer.Remarks = CObj.Remarks;// "TEstINg";
                    oCustomer.DeliveryInstructions = CObj.DeliveryInstructions;
                    oCustomer.PromptPaymentDiscount = CObj.PromptPaymentDiscount;// "test payment";
                    //oCustomer.WithholdingAmountWithheldMinimumThreshold = CObj.WithholdingMinimumThreshold;// .10;
                    oCustomer.OutlookFolder = CObj.OutlookFolder;// "outlook";
                    oCustomer.DocumentFolder = CObj.DocumentFolder;// "outlook";
                    oCustomer.AnalysisCodeId = CObj.AnalysisCodeId;
                    oCustomer.SalesLocationId = CObj.SalesLocationId;
                    oCustomer.SalesRepresentativeId = CObj.SalesRepresentativeId;
                    // oCustomer.WithholdingAmountWithheldMethod = Utility.GetWithholdingAmountWithheld(CObj.WithholdingMethodId); //WithholdingAmountWithheldMethod.Always;                                


                    if (oCustomer.BrokenRulesCollection.Count > 0)
                    {
                        StringBuilder errorString = new StringBuilder();
                        result.Status = HttpStatusCode.ExpectationFailed;
                        foreach (var brokenRule in oCustomer.BrokenRulesCollection)
                        {
                            errorString.Append(brokenRule.Description);
                            errorString.AppendLine();
                            // result.Message += brokenRule.RuleName + "\n";
                        }
                        result.Message = errorString.ToString();
                        result.Data = string.Empty;
                        log.WriteErrorLogs(result.Message);
                    }
                    else
                    {
                        oCustomer = oCustomer.Save();
                        result.Status = HttpStatusCode.OK;
                        result.Data = oCustomer.Id.ToString();
                        result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                        log.WriteErrorLogs("Login Successful");
                    }
                }
                else
                {
                    result.Status = HttpStatusCode.Unauthorized;
                    result.Message = "Login failed";//.Id.ToString();
                    result.Data = string.Empty;
                    log.WriteErrorLogs("Login Failed");
                }
                Principal.LogOut();
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return result;
        }

        public ResponseResult InsertCustomerAddress(DeliveryAddress1 DAObj)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
                {
                    using (SqlCommand _cmd = new SqlCommand(ATConstants.DAddress.StoredProcedure.InsertCustomerAddress, _con))
                    {
                        _cmd.CommandType = CommandType.StoredProcedure;
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.CustomerId, DAObj.CustomerId));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.AddressTypeId, DAObj.AddressTypeId));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.Street, DAObj.Street));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.State, DAObj.State));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.Suburb, DAObj.Suburb));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.Country, DAObj.Country));
                        _cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.PostCode, DAObj.PostCode));
                        //_cmd.Parameters.Add(new SqlParameter(ATConstants.DAddress.StoredProcedure.Parameters.IsPrimary, DAObj.IsPrimary));
                        _con.Open();

                        int nid;
                        SqlDataReader reader = _cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            reader.Read();
                            nid = Convert.ToInt32(reader[0]);

                            result.Status = HttpStatusCode.OK;
                            result.Data = nid.ToString();
                            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                        }
                        else
                        {
                            result.Message = AT_BusinessLayer.ATConstants.ResponseType.Failure;
                        }
                        _con.Close();
                    }
                }
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return result;
        }

        #endregion

        #region "Supplier"

        public SupplierInfoList GetSupplier()
        {
            try
            {
                Utility.IsLoginToSybiz();
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return SupplierInfoList.GetList();
        }

        public List<SupplierModel> GetCreatedSuppliersFromDate(string lastSyncDate)
        {
            List<SupplierModel> suppliers = new List<SupplierModel>();

            using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
            {
                using (SqlCommand _cmd = new SqlCommand(ATConstants.Supplier.StoredProcedure.GetCreatedSuppliers, _con))
                {
                    _cmd.CommandType = CommandType.StoredProcedure;
                    _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                    _con.Open();
                    _cmd.CommandTimeout = 1000;

                    using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                    {
                        DataTable oDataTable = new DataTable();
                        a.Fill(oDataTable);
                        suppliers = Utility.DataReaderMapToList<SupplierModel>(oDataTable);
                    }
                }
            }
            return suppliers;
        }

        public List<SupplierModel> GetDeletedSuppliersFromDate(string lastSyncDate)
        {
            List<SupplierModel> suppliers = new List<SupplierModel>();

            using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
            {
                using (SqlCommand _cmd = new SqlCommand(ATConstants.Supplier.StoredProcedure.GetDeletedSuppliers, _con))
                {
                    _cmd.CommandType = CommandType.StoredProcedure;
                    _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.LastSyncDate, lastSyncDate));
                    _con.Open();
                    _cmd.CommandTimeout = 1000;
                    using (SqlDataAdapter a = new SqlDataAdapter(_cmd))
                    {
                        DataTable oDataTable = new DataTable();
                        a.Fill(oDataTable);
                        suppliers = Utility.DataReaderMapToList<SupplierModel>(oDataTable);
                    }
                }
            }
            return suppliers;
        }

        //public void CreateSupplier()
        //{
        //    //DataPortal portal = new DataPortal.creae

        //    //portal.Create(Supplier,null,new DataPortalContext(Principal.CurrentPrincipal(),false);
        //    //SupplierInfo supplier =  SupplierInfoList.GetList().AddNew();
        //    //SupplierInfo supplier = new SupplierInfo();
        //    //Supplier.GetObject(0);

        //    //sup.Code = "test";
        //    Utility.LoginToSybiz();
        //    //if (SupplierInfoList.GetList().AllowNew == val)
        //    //{
        //    //    return SupplierInfoList.GetList().AddNew();

        //    //}
        //    ////supplier.Name = "abc";
        //    ////supplier.TradingName = "abc";
        //    ////SupplierInfoList.GetList().Add(supplier);
        //    //return SupplierInfoList.GetList().Last();
        //    //SupplierInfoList.GetList().AddNew().Name.Insert(0,"TestName");
        //}

        public ResponseResult CreateSupplier(SupplierModel SObj)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                if (Utility.IsLoginToSybiz())
                {
                    Supplier oSupplier = Supplier.GetObject(0);
                    oSupplier.Code = SObj.Code;// "test123";
                    oSupplier.Email = SObj.Email;// "abc@gmail.com";
                    oSupplier.Fax = SObj.Fax;// string.Empty;
                    oSupplier.Group = SObj.GroupId;// 1;
                    oSupplier.IsActive = SObj.IsActive;// false;
                    oSupplier.Name = SObj.Name;// "Daffodil Test";
                    oSupplier.SortCodeId = SObj.SortCodeId;// 76;
                    oSupplier.TradingName = SObj.TradingName;// "abc";
                    oSupplier.TradingTerms = SObj.TradingTermId;// 2;
                    oSupplier.Website = SObj.Website;// string.Empty;
                    oSupplier.TaxStatus = Utility.GetSupplierTaxStatus(SObj.TaxStatusId);
                    oSupplier.Telephone1 = SObj.Telephone1;

                    //Supplier Physical Address
                    oSupplier.PhysicalAddress.Country = SObj.PhysicalCountry;
                    oSupplier.PhysicalAddress.PostCode = SObj.PhysicalPostCode;
                    oSupplier.PhysicalAddress.State = SObj.PhysicalState;
                    oSupplier.PhysicalAddress.Street = SObj.PhysicalStreet;
                    oSupplier.PhysicalAddress.Suburb = SObj.PhysicalSuburb;
                    //oSupplier.p = SObj.PhysicalSuburb;
                    //Supplier Postal Address
                    oSupplier.PostalAddress.Country = oSupplier.PostalCountry;
                    oSupplier.PostalAddress.PostCode = oSupplier.PostalPostCode;
                    oSupplier.PostalAddress.State = oSupplier.PostalState;
                    oSupplier.PostalAddress.Street = oSupplier.PostalStreet;
                    oSupplier.PostalAddress.Suburb = oSupplier.PostalSuburb;


                    oSupplier.Telephone2 = SObj.Telephone2;
                    oSupplier.TaxNumber = SObj.TaxNumber;// "55 926 289 191";
                    oSupplier.Remarks = SObj.Remarks;// "TEstINg";
                    oSupplier.DeliveryInstructions = SObj.DeliveryInstructions;
                    oSupplier.PromptPaymentText = SObj.PromptPaymentText;// "test payment";
                    oSupplier.WithholdingAmountWithheldMinimumThreshold = SObj.WithholdingMinimumThreshold;// .10;
                    oSupplier.OutlookFolder = SObj.OutlookFolder;// "outlook";
                    oSupplier.DocumentFolder = SObj.DocumentFolder;// "outlook";
                    oSupplier.AnalysisCodeId = SObj.AnalysisCodeId;
                    oSupplier.PurchaseLocationId = SObj.PurchaseLocationId;
                    oSupplier.WithholdingAmountWithheldMethod = Utility.GetWithholdingAmountWithheld(SObj.WithholdingMethodId); //WithholdingAmountWithheldMethod.Always;
                    //oSupplier.WithholdingAmountWithheldMethod=SObj
                    //oSupplier.PhysicalState = SObj.PhysicalState;
                    if (oSupplier.BrokenRulesCollection.Count > 0)
                    {
                        StringBuilder errorString = new StringBuilder();
                        result.Status = HttpStatusCode.ExpectationFailed;
                        foreach (var brokenRule in oSupplier.BrokenRulesCollection)
                        {
                            errorString.Append(brokenRule.Description);
                            errorString.AppendLine();
                            result.Message += brokenRule.RuleName + "\n";
                        }
                        result.Message = errorString.ToString();
                        result.Data = string.Empty;
                        log.WriteErrorLogs(result.Message);
                    }
                    else
                    {
                        oSupplier = oSupplier.Save();
                        result.Status = HttpStatusCode.OK;
                        result.Data = oSupplier.Id.ToString();
                        result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                        log.WriteErrorLogs("Login Successful");
                    }
                }
                else
                {
                    result.Status = HttpStatusCode.Unauthorized;
                    result.Message = "Login failed";//.Id.ToString();
                    result.Data = string.Empty;
                    log.WriteErrorLogs("Login Failed");
                }
                Principal.LogOut();
            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return result;
            //using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
            //{
            //    using (SqlCommand _cmd = new SqlCommand(ATConstants.Supplier.StoredProcedure.InsertSuppliers, _con))
            //    {
            //        _cmd.CommandType = CommandType.StoredProcedure;
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Code, SObj.Code));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Name, SObj.Name));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.TradingName, SObj.TradingName));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.SortCodeId, SObj.SortCodeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.IsActive, SObj.IsActive));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.GroupId, SObj.GroupId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.CreditLimit, SObj.CreditLimit));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.TaxNumber, SObj.TaxNumber));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Remarks, SObj.Remarks));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.TradingTermId, SObj.TradingTermId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.AccountingMethodId, SObj.AccountingMethodId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.PromptPaymentDiscount, SObj.PromptPaymentDiscount));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.PromptPaymentText, SObj.PromptPaymentText));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.WithholdingMethodId, SObj.WithholdingMethodId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.WithholdingRate, SObj.WithholdingRate));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.WithholdingMinimumThreshold, SObj.WithholdingMinimumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.WithholdingMaximumThreshold, SObj.WithholdingMaximumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.VoluntaryMaximumThreshold, SObj.VoluntaryMaximumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.VoluntaryMinimumThreshold, SObj.VoluntaryMinimumThreshold));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.VoluntaryRate, SObj.VoluntaryRate));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.VoluntaryUsed, SObj.VoluntaryUsed));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.OutlookFolder, SObj.OutlookFolder));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.DocumentFolder, SObj.DocumentFolder));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Website, SObj.Website));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Notes, SObj.Notes));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Telephone1, SObj.Telephone1));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Telephone2, SObj.Telephone2));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Email, SObj.Email));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.Fax, SObj.Fax));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.AnalysisCodeId, SObj.AnalysisCodeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.PurchaseLocationId, SObj.PurchaseLocationId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.ReferenceNumberRequired, SObj.ReferenceNumberRequired));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.CreatedBy, SObj.CreatedBy));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.TaxStatusId, SObj.TaxStatusId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.TaxCodeId, SObj.TaxCodeId));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.LastModifiedBy, SObj.LastModifiedBy));                    
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.DeliveryInstructions, SObj.DeliveryInstructions));
            //        _cmd.Parameters.Add(new SqlParameter(ATConstants.Supplier.StoredProcedure.Parameters.BlockInvoicePayments, SObj.BlockInvoicePayments));


            //        _con.Open();
            //        _cmd.CommandTimeout = 1000;
            //        _cmd.ExecuteNonQuery();
            //        _con.Close();
            //    }
            //}

        }


        #endregion

        //public void CreateSalesOrder(SalesOrderModel SOObj)
        //{
        //    Utility.IsLoginToSybiz();
        //    using (SqlConnection _con = new SqlConnection(Utility.GetConnectionString()))
        //    {
        //        using (SqlCommand _cmd = new SqlCommand(ATConstants.Product.StoredProcedure.InsertSalesOrder, _con))
        //        {
        //            _cmd.CommandType = CommandType.StoredProcedure;
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.AnalysisCodeId, SOObj.AnalysisCodeId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.CreatedBy, SOObj.CreatedBy));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.CurrencyId, SOObj.CurrencyId));
        //            //_cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.CustomerId, SOObj.CustomerId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.DeliveryAddressId, SOObj.DeliveryAddressId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.DeliveryCustomerId, SOObj.DeliveryCustomerId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.DeliveryInstructions, SOObj.DeliveryInstructions));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.DiscountPercentage, SOObj.DiscountPercentage));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.ExchangeRate, SOObj.ExchangeRate));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.ExchangeRateIsFixed, SOObj.ExchangeRateIsFixed));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.ForeignTotal, SOObj.ForeignTotal));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.InvoiceCustomerId, SOObj.InvoiceCustomerId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.IsPosted, SOObj.IsPosted));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.LastModifiedBy, SOObj.LastModifiedBy));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.LocationId, SOObj.LocationId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.Notes, SOObj.Notes));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.OrderCustomerId, SOObj.OrderCustomerId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.OrderStatus, SOObj.OrderStatus));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.ProjectId, SOObj.ProjectId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.Reference, SOObj.Reference));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SalesOrderNumber, SOObj.SalesOrderNumber));
        //            //_cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SalesOrderId, SOObj.SalesOrderId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SalesRepresentativeId, SOObj.SalesRepresentativeId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.SystemExchangeRate, SOObj.SystemExchangeRate));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.TaxInclusive, SOObj.TaxInclusive));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.Total, SOObj.Total));
        //            //_cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.TransactionDate, SOObj.TransactionDate));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.UserId, SOObj.UserId));
        //            _cmd.Parameters.Add(new SqlParameter(ATConstants.Product.StoredProcedure.Parameters.AutoGenerated, SOObj.AutoGenerated));


        //            _con.Open();
        //            _cmd.CommandTimeout = 1000;
        //            _cmd.ExecuteNonQuery();
        //            _con.Close();
        //        }
        //    }

        //}

        public ResponseResult CreateSalesOrder(SalesOrderModel SOObj)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                if (Utility.IsLoginToSybiz())
                {
                    
                    log.WriteErrorLogs("Sybiz Products Count" + SOObj.Products.Count);
                    SalesOrder oSalesOrder = SalesOrder.GetObject(null, 0);
                    oSalesOrder.Customer = SOObj.CustomerId;
                    //oSalesOrder.AnalysisCode = SOObj.AnalysisCodeId;

                    if (SOObj.Products.Count > 0)
                    {
                        log.WriteErrorLogs("Products Count" + SOObj.Products.Count);
                        foreach (SalesOrderLineModel sol in SOObj.Products)
                        {
                            SalesOrderLine line = oSalesOrder.Lines.AddNew();

                            if (sol.Account == "Freight Outwards - Local")
                            {
                                line.Account = 55;
                                line.AccountType = TransactionLineAccountType.GL;
                            }
                            else
                            {
                                line.AccountType = TransactionLineAccountType.IC;
                                log.WriteErrorLogs("Account Count" + sol.Account);
                                line.Account = Utility.GetProductIdFromSKU(sol.Account);
                                line.TaxCode = sol.TaxCode;
                                line.SalesRepresentative = SOObj.SalesRepresentativeId;
                                line.AnalysisCode = SOObj.AnalysisCodeId;
                                line.DeliveryDate = sol.DeliveryDate;
                                //line.Description = sol.Description;
                                line.Description = Utility.GetProductDescriptionBySKU(sol.Account);
                                line.DiscountPercentage = SOObj.DiscountPercentage;
                                line.Location = sol.Location;
                                line.Quantity = sol.Quantity;
                                line.QuantityOrder = sol.QuantityOrder;
                                line.UnitChargeExclusive = sol.UnitChargeExclusive;
                                line.UnitChargeInclusive = sol.UnitChargeInclusive;
                                line.UnitOfMeasure = sol.UnitOfMeasure;
                            }
                        }
                    }
                    oSalesOrder.PriceEntryMode = SOObj.TaxInclusive ? TransactionPriceMode.Inclusive : TransactionPriceMode.Exclusive;
                    oSalesOrder.DeliveryCustomer = SOObj.DeliveryCustomerId;
                    oSalesOrder.Reference = SOObj.Reference;
                    oSalesOrder.DiscountPercentage = SOObj.DiscountPercentage;
                    oSalesOrder.Notes = SOObj.Notes;

                    if (oSalesOrder.BrokenRulesCollection.Count > 0)
                    {
                        StringBuilder errorString = new StringBuilder();
                        result.Status = HttpStatusCode.ExpectationFailed;
                        foreach (var brokenRule in oSalesOrder.BrokenRulesCollection)
                        {
                            errorString.Append(brokenRule.Description);
                            errorString.AppendLine();
                            result.Message += brokenRule.RuleName + "\n";
                        }
                        result.Message = errorString.ToString();
                        result.Data = string.Empty;
                    }
                    else
                    {
                        log.WriteErrorLogs("Sales Order Saving");

                        if (oSalesOrder.IsProcessable)
                        {
                            //ProcessingEngine.Execute(oSalesOrder);
                            oSalesOrder.Process();

                        }
                        else if (oSalesOrder.IsSavable)
                        {
                            oSalesOrder = oSalesOrder.Save();
                        }

                        log.WriteErrorLogs("Sales Order Saved");
                        result.Status = HttpStatusCode.OK;
                        result.Data = oSalesOrder.Id.ToString();
                        result.Message = AT_BusinessLayer.ATConstants.ResponseType.Success;
                        log.WriteErrorLogs("Login Successful");
                    }
                }
                else
                {
                    result.Status = HttpStatusCode.Unauthorized;
                    result.Message = "Login failed";//.Id.ToString();
                    result.Data = string.Empty;
                    log.WriteErrorLogs("Login Failed");
                }
                Principal.LogOut();

            }
            catch (Exception e)
            {
                log.WriteErrorLogs(e);
            }
            return result;
        }


        //public SalesOrderLineList GetSalesOrder()
        //{
        //    try
        //    {
        //        Utility.IsLoginToSybiz();
        //    }
        //    catch (Exception e)
        //    {
        //        log.WriteErrorLogs(e);
        //    }

        //    return SalesO
        //}
    }
}
